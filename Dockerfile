FROM ubuntu:latest

LABEL maintainer="cpunchline@foxmail.com"
LABEL version="0.0.1"
LABEL description="This is custom Docker Image for the C/C++ Services by cpunchline."
ARG DEBIAN_FRONTEND=noninteractive

WORKDIR /home/ubuntu

RUN  apt-get update && apt-get upgrade
RUN  apt-get -y install build-essential gdb cmake ccache pkgconf ninja-build
RUN  apt-get -y install vim
RUN  apt-get -y install clangd clang-format clang-tidy
RUN  apt-get -y install git curl wget vim tree zip rar unrar
RUN  apt-get -y install cloc cppcheck

# docker build -t ubuntu:cpunchline .
