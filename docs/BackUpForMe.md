### 教程

- [Markdown 官方教程](https://markdown.com.cn "Markdown 官方教程")
- [CMake 中文文档](https://cmake-doc.readthedocs.io/zh-cn/latest/index.html "CMake 3.26.4 Documentation")
- [clangd 官方介绍](https://clangd.llvm.org "clangd 官方介绍")
- [C/C++ 参考手册](https://zh.cppreference.com/ "cppreference")
- [Hello 算法](https://www.hello-algo.com/chapter_hello_algo/ "Hello 算法")

### 资源

- [Linux内核源码](https://elixir.bootlin.com/linux/latest/source "在线阅读 Linux内核源码")
- [Compiler Explorer](https://godbolt.org/ "在线C/C++编译")
- [KMS 地址列表](https://www.coolhub.top/tech-articles/kms_list.html "KMS 地址列表")
- [Source Insiht 中文版](https://pan.baidu.com/share/init?surl=3hLHAMRbI95wYuO-AHhWvw "密码1234")
- [winlibs_mingw](https://github.com/brechtsanders/winlibs_mingw/releases "winlibs_mingw")

### 博客

- [weharmonyos](https://weharmonyos.com "weharmonyos")
- [爱编程的大丙](https://subingwen.cn "爱编程的大丙")
