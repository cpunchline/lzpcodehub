### 采用CMake submodule机制, 子模块也有依赖, 则递归地拉取代码
  - 添加子模块
    - googletest库: `git submodule add https://gitee.com/mirrors/googletest.git thirdparty/gtest`
    - cJSON库: `git submodule add https://gitee.com/mirrors/cJSON.git thirdparty/cJSON`
    - rapidjson库: `git submodule add https://gitee.com/mirrors/rapidjson.git thirdparty/rapidjson`
    - jsoncpp库: `git submodule add https://gitee.com/mirrors/jsoncpp.git thirdparty/jsoncpp`
    - mysql-connector-cpp库: `git submodule add https://gitee.com/mirrors/mysql-connector-cpp.git thirdparty/mysql-connector-cpp`
    - uthash库: `git submodule add https://gitee.com/mirrors/uthash.git thirdparty/uthash`
    - libhv库: `git submodule add https://gitee.com/mirrors/libhv.git thirdparty/libhv`
    - libfastcommon库: `git submodule add https://gitee.com/mirrors/libfastcommon.git thirdparty/libfastcommon`
    - c_util库: `git submodule add https://gitee.com/ezzuodp/c_util.git thirdparty/c_util`
    - mult_timer库: `git submodule add https://gitee.com/simpost/mult_timer.git thirdparty/mult_timer`
    - EFSMC库: `git submodule add https://gitee.com/simpost/EFSMC.git thirdparty/EFSMC`
    - ERPC-doc库: `git submodule add https://gitee.com/simpost/ERPC-doc.git thirdparty/ERPC-doc`
    - data-structures-c库: `git submodule add https://gitee.com/cyberdash/data-structures-c.git thirdparty/data-structures-c`
    - data-structures-cpp库: `git submodule add https://gitee.com/cyberdash/data-structures-cpp.git thirdparty/data-structures-cpp`
    - iniparser库: `git submodule add https://gitee.com/mirrors/iniparser.git thirdparty/iniparser`
    - nng库: `git submodule add https://gitee.com/mirrors/nng.git thirdparty/nng`
    - libzmq库: `git submodule add  https://gitee.com/mirrors/libzmq.git thirdparty/libzmq`
    - czmq库: `git submodule add https://github.com/zeromq/czmq.git thirdparty/czmq`
    - melon库: `git submodule add https://gitee.com/mirrors/melon.git thirdparty/melon`
    - tcmalloc库: `git submodule add https://gitee.com/mirrors/tcmalloc.git thirdparty/tcmalloc`
    - magic_enum库: `git submodule add https://github.com/Neargye/magic_enum.git thirdparty/magic_enum`
    - libubox库: `git submodule add https://gitee.com/git-openwrt/libubox.git thirdparty/libubox`
    - cpp-tbox库: `git submodule add https://gitee.com/cpp-master/cpp-tbox.git thirdparty/cpp-tbox`
    - cpp-ipc库(在Linux/Windows上使用共享内存的高性能进程间通信库): `git submodule add https://github.com/mutouyun/cpp-ipc thirdparty/cpp-ipc`
    - shell脚本实现的解析ini的库 `https://github.com/wallyhall/shini`
    - concurrentqueue基于C++11的线程间无锁队列库 `git submodule add https://gitee.com/mirrors/concurrentqueue.git thirdparty/concurrentqueue`
    - gear-lib 适用于IOT/嵌入式/网络服务开发的C库 `git submodule add https://gitee.com/gozfreee/gear-lib thirdparty/gear-lib`
    - erez 线程间无锁队列 `git submodule add https://github.com/erez-strauss/lockfree_mpmc_queue thirdparty/lockfree_mpmc_queue`
  - 查看子模块
    - `git submodule`
  - 拉取子模块
    - `git submodule update --init --recursive` 拉取没带上述选项, 则执行该命令; 所有层层嵌套的子模块
  - 更新子模块: 进入子模块目录查看和拉取即可; 为了直接从子模块的当前分支的远程追踪分支获取最新变更, 不加则是默认从父项目的 SHA-1 记录中获取变更
    - `git submodule update --remote --merge` 父模块目录执行
  - 移除子模块
    - `git submodule deinit -f /path/to/submodule`
    - `git rm -f /path/to/submodule`
    - `rm -rf .git/modules/path/to/submodule`
