set(SHMQUEUE_APP shmqueue_app)

set(SHMQUEUE_SOURCE_FILES main.cpp
    shmmqueue.cpp
    shmmqueue.h
    shm_rwlock.h
    shm_rwlock.cpp)
add_executable(${SHMQUEUE_APP} ${SHMQUEUE_SOURCE_FILES})
target_link_libraries(${SHMQUEUE_APP} pthread)

install(TARGETS ${SHMQUEUE_APP} DESTINATION bin)
