set(THREADQUEUE_APP threadqueue_app)

aux_source_directory(. COROUTINE_APP_SRC_LIST)

add_executable(${THREADQUEUE_APP} ${COROUTINE_APP_SRC_LIST})
target_include_directories(${THREADQUEUE_APP} PUBLIC .)

install(TARGETS ${THREADQUEUE_APP} DESTINATION bin)
