#include "shm.h"

#include <sys/types.h>
#include <sys/shm.h>
#include <sys/ipc.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#ifndef MIN
#define MIN(x, y)                      \
    ({                                 \
        typeof(x) _min1 = (x);         \
        typeof(y) _min2 = (y);         \
        (void)(&_min1 == &_min2);      \
        _min1 < _min2 ? _min1 : _min2; \
    })
#endif

static int32_t s_shm_header_align_size = SHM_SIZE(sizeof(SHM_HEADER));

// clang-format off
/* the count of blocks should less than SHM_BLOCK_COUNT_MAX */
static SHM_BLOCK_INFO s_shm_blocks[] = {
    {SHM_BLOCK_ID_TEST, sizeof(SHM_BLOCK_TEST), SHM_SIZE(sizeof(SHM_BLOCK_TEST))},
};
static unsigned int s_shm_blocks_count = sizeof(s_shm_blocks) / sizeof(s_shm_blocks[0]);
// clang-format on

SHM_BLOCK_INFO *get_shm_blocks()
{
    return s_shm_blocks;
}

unsigned int get_shm_blocks_count()
{
    return s_shm_blocks_count;
}

unsigned int get_shm_align_header_size()
{
    return s_shm_header_align_size;
}

unsigned int get_shm_real_block_size(unsigned int block_id)
{
    if (block_id >= s_shm_blocks_count)
    {
        /* block_id error */
        return SHM_BLOCK_SIZE_INVALID;
    }
    return (s_shm_blocks[block_id].block_size);
}

unsigned int get_shm_align_block_size(unsigned int block_id)
{
    if (block_id >= s_shm_blocks_count)
    {
        /* block_id error */
        return SHM_BLOCK_SIZE_INVALID;
    }
    return s_shm_blocks[block_id].align_block_size;
}

unsigned int get_shm_align_blocks_size()
{
    unsigned int total_align_size = 0;
    for (unsigned int i = 0; i < s_shm_blocks_count; i++)
    {
        total_align_size += s_shm_blocks[i].align_block_size;
    }
    return total_align_size;
}

unsigned int get_shm_align_total_size()
{
    return s_shm_header_align_size + get_shm_align_blocks_size();
}

/* share memory attach ptr */
static void *s_shmem = (void *)-1;

static int shm_header_init(SHM_HEADER *shm_header_ptr)
{
    unsigned int i = 0;
    unsigned int offset = 0; // mean begin of shm_header_ptr->shm_data 's offset
    pthread_mutexattr_t mutex_attr;
    int ret = -1;

    pthread_mutexattr_init(&mutex_attr);
    pthread_mutexattr_setpshared(&mutex_attr, PTHREAD_PROCESS_SHARED);
    pthread_mutexattr_setrobust(&mutex_attr, PTHREAD_MUTEX_ROBUST);

    memset((void *)shm_header_ptr, 0, sizeof(SHM_HEADER));
    pthread_mutex_init(&(shm_header_ptr->mutex), &mutex_attr);

    int lock_err = pthread_mutex_lock(&(shm_header_ptr->mutex));
    if (lock_err == EOWNERDEAD)
    {
        ret = pthread_mutex_consistent(&(shm_header_ptr->mutex));
        if (0 != ret)
        {
            printf("pthread_mutex_consistent fail, ret[%d], errno[%d](%s)", ret, errno, strerror(errno));
            // 后续如果有其他进程再次尝试加锁收到ENOTRECOVERABLE错误码, 此时在这种情况下, 应该销毁并重新初始化互斥锁
        }
    }
    else if (lock_err < 0)
    {
        printf("pthread_mutex_lock fail, ret[%d], errno[%d](%s)", ret, errno, strerror(errno));
        return -1;
    }
    shm_header_ptr->magic_num = SHM_MAGIC_NUM;
    shm_header_ptr->refcount++;
    shm_header_ptr->block_count = get_shm_blocks_count();
    shm_header_ptr->total_size = get_shm_align_total_size();

    /* init blocks and create mutex among processes */
    for (i = 0; i < shm_header_ptr->block_count; i++)
    {
        pthread_mutex_init(&(shm_header_ptr->block[i].mutex), &mutex_attr);
        shm_header_ptr->block[i].offset = offset;
        shm_header_ptr->block[i].block_size = get_shm_align_block_size(i);
        memset(shm_header_ptr->shm_data + shm_header_ptr->block[i].offset, 0x00, shm_header_ptr->block[i].block_size);
        offset += shm_header_ptr->block[i].block_size;
    }
    pthread_mutex_unlock(&(shm_header_ptr->mutex));
    pthread_mutexattr_destroy(&mutex_attr);

    return 0;
}

/**
 * @brief shm init
 * @return int : 0 is success , other failed
 */
int shm_init()
{
    if (s_shmem != (void *)-1)
    {
        /* share memory already be created */
        return 0;
    }

    /* create a key */
    const char *filepath = "/";
    key_t key = ftok(filepath, 1);

    /* create share memory */
    unsigned int total_size = get_shm_align_total_size();
    int shmid = shmget(key, total_size, SHM_RIGHT | IPC_CREAT);
    if (shmid == -1)
    {
        return -1;
    }

    /* attach share memory */
    s_shmem = shmat(shmid, 0, 0);
    if (s_shmem == (void *)-1)
    {
        return -1;
    }

    /* convert to SHM_HEADER type */
    SHM_HEADER *shm_header_ptr = (SHM_HEADER *)s_shmem;
    if (SHM_MAGIC_NUM != shm_header_ptr->magic_num || 0 == shm_header_ptr->refcount)
    {
        // only the dm service should goto here
        shm_header_init(shm_header_ptr);
    }
    else
    {
        int ret = -1;
        int lock_err = pthread_mutex_lock(&(shm_header_ptr->mutex));
        if (lock_err == EOWNERDEAD)
        {
            ret = pthread_mutex_consistent(&(shm_header_ptr->mutex));
            if (0 != ret)
            {
                printf("pthread_mutex_consistent fail, ret[%d], errno[%d](%s)", ret, errno, strerror(errno));
                // 后续如果有其他进程再次尝试加锁收到ENOTRECOVERABLE错误码, 此时在这种情况下, 应该销毁并重新初始化互斥锁
            }
        }
        else if (lock_err < 0)
        {
            printf("pthread_mutex_lock fail, ret[%d], errno[%d](%s)", ret, errno, strerror(errno));
            return -1;
        }
        shm_header_ptr->refcount++;
        pthread_mutex_unlock(&(shm_header_ptr->mutex));
    }

    return 0;
}

/**
 * @brief lock one block of shm.
 * @param[in] block_id : the block id of shm
 * @return int : 0 is success , other failed
 */
int shm_lock(unsigned int block_id)
{
    if (s_shmem == (void *)-1)
    {
        return -1;
    }

    if (block_id >= get_shm_blocks_count())
    {
        return -1;
    }

    SHM_HEADER *shm_header_ptr = (SHM_HEADER *)s_shmem;
    int ret = -1;
    int lock_err = pthread_mutex_lock(&(shm_header_ptr->block[block_id].mutex));
    if (lock_err == EOWNERDEAD)
    {
        ret = pthread_mutex_consistent(&(shm_header_ptr->block[block_id].mutex));
        if (0 != ret)
        {
            printf("pthread_mutex_consistent fail, ret[%d], errno[%d](%s)", ret, errno, strerror(errno));
            // 后续如果有其他进程再次尝试加锁收到ENOTRECOVERABLE错误码, 此时在这种情况下, 应该销毁并重新初始化互斥锁
        }
    }
    else if (lock_err < 0)
    {
        printf("pthread_mutex_lock fail, ret[%d], errno[%d](%s)", ret, errno, strerror(errno));
        return -1;
    }

    return 0;
}

/**
 * @brief unlock one block of shm.
 * @param[in] block_id : the block id of shm
 * @return int : 0 is success , other failed
 */
int shm_unlock(unsigned int block_id)
{
    if (s_shmem == (void *)-1)
    {
        return -1;
    }
    if (block_id >= get_shm_blocks_count())
    {
        return -1;
    }

    SHM_HEADER *shm_header_ptr = (SHM_HEADER *)s_shmem;
    pthread_mutex_unlock(&(shm_header_ptr->block[block_id].mutex));

    return 0;
}

/**
 * @brief get addr of one block data.
 * You should call shm_lock() before call shm_get_block_addr(), and call shm_unlock() after.
 * @param[in] block_id : the block id of shm
 * @return void * : if fail, return NULL; if success, return the addr of the block data.
 */
void *shm_get_block_addr(unsigned int block_id)
{
    if (block_id >= get_shm_blocks_count())
    {
        return NULL;
    }

    SHM_HEADER *shm_header_ptr = (SHM_HEADER *)s_shmem;
    unsigned int offset = shm_header_ptr->block[block_id].offset;

    return (void *)(shm_header_ptr->shm_data + offset);
}

/**
 * @brief copy one block data from shm to usr buffer.
 * @param[in] block_id : the block id of shm
 * @param[in] data_len : length of data_buf
 * @param[out] data_buf : usr buffer
 * @return int : 0 is success , other failed
 */
int shm_get_block_data(unsigned int block_id, unsigned int data_len, void *data_buf)
{
    if (block_id >= get_shm_blocks_count())
    {
        return -1;
    }

    int ret = shm_lock(block_id);
    if (0 != ret)
    {
        return ret;
    }

    unsigned int real_block_size = get_shm_real_block_size(block_id);
    if (SHM_BLOCK_SIZE_INVALID == real_block_size)
    {
        return -1;
    }
    unsigned int copy_len = MIN(data_len, real_block_size);
    SHM_HEADER *shm_header_ptr = (SHM_HEADER *)s_shmem;
    unsigned int offset = shm_header_ptr->block[block_id].offset;
    memcpy(data_buf, shm_header_ptr->shm_data + offset, copy_len);
    shm_unlock(block_id);

    return 0;
}

/**
 * @brief copy one block data from usr buffer to shm.
 * @param[in] block_id : the block id of shm
 * @param[in] data_len : length of data_buf
 * @param[out] data_buf : usr buffer
 * @return int : 0 is success , other failed
 */
int shm_set_block_data(unsigned int block_id, unsigned int data_len, void *data_buf)
{
    if (block_id >= get_shm_blocks_count())
    {
        return -1;
    }
    unsigned int real_block_size = get_shm_real_block_size(block_id);
    if (SHM_BLOCK_SIZE_INVALID == real_block_size || data_len > real_block_size)
    {
        return -1;
    }

    int ret = shm_lock(block_id);
    if (0 != ret)
    {
        return ret;
    }

    SHM_HEADER *shm_header_ptr = (SHM_HEADER *)s_shmem;
    unsigned int offset = shm_header_ptr->block[block_id].offset;
    memset(shm_header_ptr->shm_data + offset, 0, shm_header_ptr->block[block_id].block_size);
    memcpy(shm_header_ptr->shm_data + offset, data_buf, data_len);
    shm_unlock(block_id);

    return 0;
}
