#include "TimerManager.hpp" // 假设 TimerManager 的实现被放置在这个头文件中

// 创建 TimerManager 实例，指定线程池最小和最大线程数及空闲超时时间
TimerManager timer_mgr(4, 8, 500);

// 单次定时器的回调函数
void singleShotCallback(TimerManager::TimerId id)
{
    std::cout << "Timerid[" << id << "] timeout" << std::endl;
}

// 周期性定时器的回调函数
void periodicCallback(TimerManager::TimerId id)
{
    std::cout << "Timerid[" << id << "] timeout" << std::endl;
    // 模拟一些工作负载，比如网络请求或数据库查询
    std::this_thread::sleep_for(std::chrono::seconds(1));
}

int main()
{
    // 创建一个单次执行的定时器，在3秒后触发
    TimerManager::TimerId single_shot_id = timer_mgr.CreateSingleTimer(3 * 1000 * 1000, singleShotCallback);

    // 创建一个周期性定时器，首次触发在2秒后，之后每1秒触发一次
    TimerManager::TimerId periodic_id = timer_mgr.CreateCycleTimer(2 * 1000 * 1000, 1 * 1000 * 1000, periodicCallback);

    // 创建一个多执行定时器，首次触发在5秒后，之后每4秒触发一次，总共触发4次
    TimerManager::TimerId multi_id = timer_mgr.CreateMultTimer(5 * 1000 * 1000, 4 * 1000 * 1000, 4, [](TimerManager::TimerId id)
                                                               {
                                                                   std::cout << "Timerid[" << id << "] timeout" << std::endl;
                                                               });

    // 让主线程休眠足够长的时间以便观察定时器行为
    std::this_thread::sleep_for(std::chrono::seconds(15));

    // 删除多执行定时器
    timer_mgr.DeleteTimer(multi_id);

    // 等待一段时间让其他定时器有机会触发
    std::this_thread::sleep_for(std::chrono::seconds(5));

    // 主线程结束前确保所有的资源都被正确释放
    return 0;
}
