#include <errno.h>
#include <string.h>
#include <sys/timerfd.h>
#include <sys/epoll.h>
#include <sys/prctl.h>
#include <unistd.h>
#include <unordered_map>
#include <functional>
#include <iostream>
#include <chrono>
#include <future>
#include <condition_variable>

#include "../../components/ThreadPool/ThreadPool.hpp"

class TimerManager
{
public:
    using TimerId = uint32_t;
    using TimerCallback = std::function<void(TimerId)>;

    TimerManager(int min_threads = DEFAULT_THREAD_POOL_MIN_THREAD_NUM,
                 int max_threads = DEFAULT_THREAD_POOL_MAX_THREAD_NUM,
                 int max_idle_ms = DEFAULT_THREAD_POOL_MAX_IDLE_TIME) :
        epfd_(epoll_create1(EPOLL_CLOEXEC)), running_(false), next_timer_id_(0), thread_pool_(min_threads, max_threads, max_idle_ms)
    {
        if (epfd_ == -1)
        {
            throw std::runtime_error("epoll_create1 failed");
        }
        std::cout << "TimerManager start" << std::endl;
    }

    ~TimerManager()
    {
        Stop();
        if (epfd_ != -1)
        {
            close(epfd_);
        }
        std::cout << "TimerManager end" << std::endl;
    }

    TimerId CreateSingleTimer(int64_t microseconds, TimerCallback cb)
    {
        TimerId id = CreateTimer(microseconds, 0, std::move(cb), 1);

        std::cout << "Timerid[" << id << "] created" << std::endl;

        return id;
    }

    TimerId CreateCycleTimer(int64_t initial_interval, int64_t period, TimerCallback cb)
    {
        TimerId id = CreateTimer(initial_interval, period, std::move(cb), 0);
        std::cout << "Timerid[" << id << "] created" << std::endl;

        return id;
    }

    TimerId CreateMultTimer(int64_t initial_interval, int64_t period, size_t max_executions, TimerCallback cb)
    {
        TimerId id = CreateTimer(initial_interval, period, std::move(cb), max_executions);
        std::cout << "Timerid[" << id << "] created" << std::endl;

        return id;
    }

    bool DeleteTimer(TimerId id)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        auto it = timers_.find(id);
        if (it == timers_.end())
        {
            // not exist or already deleted
            return false;
        }

        std::cout << "Timerid[" << id << "] deleted" << std::endl;
        RemoveTimer(it->second.fd);
        timers_.erase(it);

        return true;
    }

private:
    TimerId CreateTimer(int64_t initial_interval, int64_t period, TimerCallback cb, size_t max_executions)
    {
        int fd = -1;
        TimerId id = UINT32_MAX;
        struct itimerspec ts = {};
        epoll_event ev = {};

        fd = timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK | TFD_CLOEXEC);
        if (-1 == fd)
        {
            throw std::runtime_error("timerfd_create failed, errno: " + std::to_string(errno) + " (" + std::string(strerror(errno)) + ")");
        }

        ts.it_value.tv_sec = initial_interval / 1000000;
        ts.it_value.tv_nsec = (initial_interval % 1000000) * 1000;
        if (period > 0)
        {
            ts.it_interval.tv_sec = period / 1000000;
            ts.it_interval.tv_nsec = (period % 1000000) * 1000;
        }
        else
        {
            ts.it_interval.tv_sec = 0;
            ts.it_interval.tv_nsec = 0;
        }

        if (-1 == timerfd_settime(fd, 0, &ts, nullptr))
        {
            close(fd);
            throw std::runtime_error("timerfd_settime failed, errno: " + std::to_string(errno) + " (" + std::string(strerror(errno)) + ")");
        }

        ev.events = EPOLLIN | EPOLLET;
        ev.data.fd = fd;
        if (-1 == epoll_ctl(epfd_, EPOLL_CTL_ADD, fd, &ev))
        {
            close(fd);
            throw std::runtime_error("epoll_ctl failed, errno: " + std::to_string(errno) + " (" + std::string(strerror(errno)) + ")");
        }

        id = next_timer_id_.fetch_add(1);
        {
            std::lock_guard<std::mutex> lock(mutex_);
            timers_[id] = {fd, period, std::make_shared<TimerCallback>(std::move(cb)), {}, 0, max_executions};
            fd_to_id_[fd] = id;
        }

        bool expected = false;
        if (running_.compare_exchange_strong(expected, true))
        {
            thread_ = std::thread(&TimerManager::Run, this);
        }

        return id;
    }

    void RemoveTimer(int fd)
    {
        if (-1 == epoll_ctl(epfd_, EPOLL_CTL_DEL, fd, nullptr))
        {
            throw std::runtime_error("epoll_ctl failed, errno: " + std::to_string(errno) + " (" + std::string(strerror(errno)) + ")");
        }
        close(fd);
        fd_to_id_.erase(fd);
    }

    void Run()
    {
        prctl(PR_SET_NAME, "TimerManager");
        std::cout << "TimerManager running" << std::endl;
        constexpr int MAX_EVENTS = 1024;
        int nfds = -1;
        int i = 0;
        epoll_event events[MAX_EVENTS] = {};

        while (running_.load())
        {
            nfds = epoll_wait(epfd_, events, MAX_EVENTS, -1);
            if (nfds == -1 && errno != EINTR)
            {
                break;
            }

            for (i = 0; i < nfds; ++i)
            {
                if (events[i].events & EPOLLIN)
                {
                    handleTimerEvent(events[i].data.fd);
                }
            }
        }
    }

    void Stop()
    {
        if (!running_.exchange(false))
        {
            return;
        }

        if (thread_.joinable())
        {
            thread_.join();
        }

        {
            std::lock_guard<std::mutex> lock(mutex_);
            for (auto &t : timers_)
            {
                std::cout << "Timerid[" << t.first << "] deleted" << std::endl;
                RemoveTimer(t.second.fd);
            }

            timers_.clear();
        }

        thread_pool_.stop();
    }

    void handleTimerEvent(int fd)
    {
        uint64_t exp = 0;
        ssize_t bytes_read = read(fd, &exp, sizeof(exp));
        if (bytes_read != sizeof(exp))
        {
            std::cout << "read failed, errno: " << errno << " (" << strerror(errno) << ")" << std::endl;
            return;
        }

        std::lock_guard<std::mutex> lock(mutex_);
        auto id_it = fd_to_id_.find(fd);
        if (id_it == fd_to_id_.end())
        {
            return;
        }

        auto timer_it = timers_.find(id_it->second);
        if (timer_it == timers_.end())
        {
            return;
        }

        TimerInfo &info = timer_it->second;
        auto cb = info.callback;

        if (info.max_executions > 0 && info.cur_executions >= info.max_executions)
        {
            return;
        }

        thread_pool_.commit([id = timer_it->first, cb]()
                            {
                                (*cb)(id);
                            });

        if (info.max_executions > 0 && ++info.cur_executions >= info.max_executions)
        {
            std::cout << "Timerid[" << timer_it->first << "] deleted" << std::endl;
            RemoveTimer(info.fd);
            timers_.erase(timer_it);
            return;
        }
    }

    struct TimerInfo
    {
        int fd;
        int64_t period;
        std::shared_ptr<TimerCallback> callback;
        struct itimerspec period_spec;
        size_t cur_executions = 0;
        size_t max_executions = 0;
    };

    int epfd_;
    std::thread thread_;
    std::mutex mutex_;
    std::atomic<bool> running_;
    std::atomic<TimerId> next_timer_id_;
    std::unordered_map<TimerId, TimerInfo> timers_;
    std::unordered_map<int, TimerId> fd_to_id_;
    ThreadPool thread_pool_;
};
