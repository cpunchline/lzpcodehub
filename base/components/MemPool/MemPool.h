#ifndef MEMPOOL_H
#define MEMPOOL_H

#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>

typedef struct _FixSizeMemPool
{
    size_t blockSize;
    size_t blockCount;
    char *pool;
    void **freeList;
    pthread_mutex_t mutex;
} FixSizeMemPool;

// 创建内存池
FixSizeMemPool *FixSizeMemPool_Create(size_t blockSize, size_t blockCount);

// 销毁内存池
void FixSizeMemPool_Destroy(FixSizeMemPool *pool);

// 分配内存块
void *FixSizeMemPool_Allocate(FixSizeMemPool *pool);

// 释放内存块
void FixSizeMemPool_Deallocate(FixSizeMemPool *pool, void *block);

#endif // MEMPOOL_H
