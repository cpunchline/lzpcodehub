#include "MemPool.h"

FixSizeMemPool *FixSizeMemPool_Create(size_t blockSize, size_t blockCount)
{
    FixSizeMemPool *pool = (FixSizeMemPool *)malloc(sizeof(FixSizeMemPool));
    if (!pool)
    {
        return NULL;
    }
    memset(pool, 0, sizeof(FixSizeMemPool));

    pool->blockSize = blockSize;
    pool->blockCount = blockCount;
    pool->pool = (char *)malloc(blockSize * blockCount);
    if (!pool->pool)
    {
        free(pool);
        return NULL;
    }

    pool->freeList = (void **)pool->pool;
    for (size_t i = 0; i < blockCount - 1; ++i)
    {
        void **currentBlock = (void **)(pool->pool + i * blockSize);
        *currentBlock = pool->pool + (i + 1) * blockSize;
    }

    void **lastBlock = (void **)(pool->pool + (blockCount - 1) * blockSize);
    *lastBlock = NULL;

    pthread_mutex_init(&pool->mutex, NULL);

    return pool;
}

void FixSizeMemPool_Destroy(FixSizeMemPool *pool)
{
    if (pool)
    {
        free(pool->pool);
        pthread_mutex_destroy(&pool->mutex);
        free(pool);
    }
}

void *FixSizeMemPool_Allocate(FixSizeMemPool *pool)
{
    if (!pool)
    {
        return NULL;
    }

    pthread_mutex_lock(&pool->mutex);
    if (pool->freeList == NULL)
    {
        pthread_mutex_unlock(&pool->mutex);
        return NULL;
    }

    void *block = pool->freeList;
    pool->freeList = (void **)(*pool->freeList);
    pthread_mutex_unlock(&pool->mutex);

    return block;
}

void FixSizeMemPool_Deallocate(FixSizeMemPool *pool, void *block)
{
    if (!pool || !block)
    {
        return;
    }

    pthread_mutex_lock(&pool->mutex);
    *(void **)block = pool->freeList;
    pool->freeList = (void **)block;
    pthread_mutex_unlock(&pool->mutex);
}
