#pragma once

#include <cstddef>
#include <iostream>
#include <mutex>

class FixSizeMemPool
{
public:
    FixSizeMemPool(std::size_t blockSize, std::size_t blockCount) :
        blockSize_(blockSize), blockCount_(blockCount), pool_(nullptr), freeList_(nullptr)
    {
        pool_ = new char[blockSize_ * blockCount_];
        freeList_ = reinterpret_cast<void **>(pool_);
        for (std::size_t i = 0; i < blockCount_ - 1; ++i)
        {
            void **currentBlock = reinterpret_cast<void **>(pool_ + i * blockSize_);
            *currentBlock = pool_ + (i + 1) * blockSize_;
        }

        void **lastBlock = reinterpret_cast<void **>(pool_ + (blockCount_ - 1) * blockSize_);
        *lastBlock = nullptr;
    }

    ~FixSizeMemPool()
    {
        delete[] pool_;
    }

    void *allocate()
    {
        std::lock_guard<std::mutex> lock(mutex_);
        if (freeList_ == nullptr)
        {
            return nullptr;
        }

        void *block = freeList_;
        freeList_ = static_cast<void **>(*freeList_);

        return block;
    }

    void deallocate(void *block)
    {
        if (block == nullptr)
        {
            return;
        }

        std::lock_guard<std::mutex> lock(mutex_);
        *static_cast<void **>(block) = freeList_;
        freeList_ = static_cast<void **>(block);
    }

private:
    std::size_t blockSize_;
    std::size_t blockCount_;
    char *pool_;
    void **freeList_;
    std::mutex mutex_;
};
