#pragma once

#include <tuple>
#include <mutex>
#include <memory>
#include <functional>
#include <condition_variable>
#include <list>

// 基于锁的内存池和队列实现(多线程可访问)

namespace lock
{

template <typename T>
class pool
{
    union node
    {
        T data_;
        node *next_;
    } *cursor_ = nullptr;

    mutable std::mutex mtx_;

public:
    ~pool()
    {
        while (cursor_ != nullptr)
        {
            auto temp = cursor_->next_;
            delete cursor_;
            cursor_ = temp;
        }
    }

    bool empty() const
    {
        auto guard = std::unique_lock{mtx_};
        return cursor_ == nullptr;
    }

    template <typename... P>
    T *alloc(P &&...pars)
    {
        auto guard = std::unique_lock{mtx_};
        if (cursor_ == nullptr)
        {
            return &((new node{std::forward<P>(pars)...})->data_);
        }
        void *p = &(cursor_->data_);
        cursor_ = cursor_->next_;
        return ::new (p) T{std::forward<P>(pars)...};
    }

    void free(void *p)
    {
        if (p == nullptr)
            return;
        auto temp = reinterpret_cast<node *>(p);
        auto guard = std::unique_lock{mtx_};
        temp->next_ = cursor_;
        cursor_ = temp;
    }
};

template <typename T>
class queue
{
    struct node
    {
        T data_;
        node *next_;
    } *head_ = nullptr,
      *tail_ = nullptr;

    pool<node> allocator_;
    mutable std::mutex mtx_;

public:
    void quit()
    {
    }

    bool empty() const
    {
        auto guard = std::unique_lock{mtx_};
        return head_ == nullptr;
    }

    bool push(T const &val)
    {
        auto p = allocator_.alloc(val, nullptr);
        auto guard = std::unique_lock{mtx_};
        if (tail_ == nullptr)
        {
            head_ = tail_ = p;
        }
        else
        {
            tail_->next_ = p;
            tail_ = p;
        }
        return true;
    }

    std::tuple<T, bool> pop()
    {
        std::unique_ptr<node, std::function<void(node *)>> temp;
        auto guard = std::unique_lock{mtx_};
        if (head_ == nullptr)
        {
            return {};
        }
        auto ret = std::make_tuple(head_->data_, true);
        temp = decltype(temp){
            head_, [this](node *temp)
            {
                allocator_.free(temp);
            }};
        head_ = head_->next_;
        if (tail_ == temp.get())
        {
            tail_ = nullptr;
        }
        return ret;
    }
};

template <typename T>
class UnBoundedQueue
{
public:
    ~UnBoundedQueue()
    {
        Quit();
    }

    void Quit()
    {
        {
            auto guard = std::unique_lock{lock_};
            quit_ = true;
        }
        cond_notEmpty_.notify_all();
    }

    bool Empty() const
    {
        auto guard = std::unique_lock{lock_};
        return list_.empty();
    }

    std::size_t Size()
    {
        auto guard = std::unique_lock{lock_};
        return list_.size();
    }

    bool Put(const T &val)
    {
        return Add(val);
    }

    bool Put(T &&val)
    {
        return Add(std::forward<T>(val));
    }

    bool Get(T &val)
    {
        auto guard = std::unique_lock{lock_};
        cond_notEmpty_.wait(lock_, [this]
                            {
                                return quit_ || !list_.empty();
                            });
        if (quit_)
        {
            return false;
        }

        val = std::move(list_.front());
        list_.pop_front();

        return true;
    }

    bool Get(std::list<T> &list)
    {
        auto guard = std::unique_lock{lock_};
        cond_notEmpty_.wait(lock_, [this]
                            {
                                return quit_ || !list_.empty();
                            });
        if (quit_)
        {
            return false;
        }

        list = std::move(list_);

        return true;
    }

    bool Peek(T &val)
    {
        auto guard = std::unique_lock{lock_};
        cond_notEmpty_.wait(lock_, [this]
                            {
                                return quit_ || !list_.empty();
                            });
        if (quit_)
        {
            return false;
        }

        val = list_.front();

        return true;
    }

private:
    std::mutex lock_;
    std::list<T> list_;
    std::condition_variable cond_notEmpty_;
    bool quit_ = false;

    template <typename F>
    bool Add(F &&f)
    {
        auto guard = std::unique_lock{lock_};
        if (quit_)
        {
            return false;
        }
        list_.emplace_back(std::forward<F>(f));
        cond_notEmpty_.notify_one();

        return true;
    }
};

template <typename T, std::size_t capacity>
class BoundedQueue
{
public:
    ~BoundedQueue()
    {
        Quit();
    }

    void Quit()
    {
        {
            auto guard = std::unique_lock{lock_};
            quit_ = true;
        }
        cond_notFull_.notify_all();
        cond_notEmpty_.notify_all();
    }

    bool Empty() const
    {
        auto guard = std::unique_lock{lock_};
        return list_.empty();
    }

    bool Full() const
    {
        auto guard = std::unique_lock{lock_};
        return list_.size() >= capacity;
    }

    std::size_t Size()
    {
        auto guard = std::unique_lock{lock_};
        return list_.size();
    }

    bool Put(const T &val)
    {
        return Add(val);
    }

    bool Put(T &&val)
    {
        return Add(std::forward<T>(val));
    }

    bool Get(T &val)
    {
        auto guard = std::unique_lock{lock_};
        cond_notEmpty_.wait(lock_, [this]
                            {
                                return quit_ || !list_.empty();
                            });
        if (quit_)
        {
            return false;
        }

        val = std::move(list_.front());
        list_.pop_front();
        cond_notFull_.notify_one();

        return true;
    }

    bool Get(std::list<T> &list)
    {
        auto guard = std::unique_lock{lock_};
        cond_notEmpty_.wait(lock_, [this]
                            {
                                return quit_ || !list_.empty();
                            });
        if (quit_)
        {
            return false;
        }

        list = std::move(list_);
        cond_notFull_.notify_one();

        return true;
    }

    bool Peek(T &val)
    {
        auto guard = std::unique_lock{lock_};
        cond_notEmpty_.wait(lock_, [this]
                            {
                                return quit_ || !list_.empty();
                            });
        if (quit_)
        {
            return false;
        }

        val = list_.front();
        cond_notFull_.notify_one();

        return true;
    }

private:
    std::mutex lock_;
    std::list<T> list_;
    std::condition_variable cond_notFull_;
    std::condition_variable cond_notEmpty_;
    bool quit_ = false;

    template <typename F>
    bool Add(F &&f)
    {
        auto guard = std::unique_lock{lock_};
        cond_notFull_.wait(lock_, [this]
                           {
                               return quit_ || list_.size() < capacity;
                           });
        if (quit_)
        {
            return false;
        }

        list_.emplace_back(std::forward<F>(f));
        cond_notEmpty_.notify_one();

        return true;
    }
};

} // namespace lock
