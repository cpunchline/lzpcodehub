#pragma once

#include <iostream>
#include <atomic>
#include <thread>
#include <functional>
#include <future>
#include <mutex>
#include <condition_variable>
#include <vector>
#include <list>
#include <queue>
#include <chrono>
#include <sstream>

#define DEFAULT_THREAD_POOL_MIN_THREAD_NUM 1
#define DEFAULT_THREAD_POOL_MAX_THREAD_NUM std::thread::hardware_concurrency()
#define DEFAULT_THREAD_POOL_MAX_IDLE_TIME  60000 // ms

class FixThreadPool
{
public:
    typedef std::function<void()> Task; // 线程池任务类型

public:
    FixThreadPool(int threadnum) :
        started_(false), threadnum_(threadnum), threadlist_(), taskqueue_(),
        mutex_(), condition_()
    {
    }

    ~FixThreadPool()
    {
        std::cout << "Clean up the FixThreadPool!" << std::endl;

        Stop();

        for (int i = 0; i < threadnum_; i++)
        {
            threadlist_[i]->join();
        }

        for (int i = 0; i < threadnum_; i++)
        {
            delete threadlist_[i];
        }

        threadlist_.clear();
    }

    // 启动线程池
    void Start()
    {
        if (threadnum_ > 0)
        {
            started_ = true;
            for (int i = 0; i < threadnum_; i++)
            {
                std::thread *pthread = new std::thread(&FixThreadPool::ThreadFunc, this);
                threadlist_.push_back(pthread);
            }
        }
        else
        {
            std::cout << "FixThreadPool: threadnum < 0!" << std::endl;
        }
    }

    // 暂停线程池
    void Stop()
    {
        started_ = false;
        condition_.notify_all();
    }

    // 添加任务
    void AddTask(Task task)
    {
        {
            std::lock_guard<std::mutex> lock(mutex_);
            taskqueue_.push(task);
        }
        // 依次唤醒等待队列的线程
        condition_.notify_one();
    }

    // 线程池执行的函数
    void ThreadFunc()
    {
        std::thread::id tid = std::this_thread::get_id();
        std::stringstream sin;
        sin << tid;
        std::cout << "Worker thread is running! tid: " << tid << std::endl;
        Task task;
        while (started_)
        {
            task = NULL;
            {
                std::unique_lock<std::mutex> lock(mutex_); // unique_lock支持解锁又上锁的情况
                while (taskqueue_.empty() && started_)
                {
                    condition_.wait(lock);
                }
                if (!started_)
                {
                    break;
                }
                std::cout << "Thread wake up! tid: " << tid << std::endl;
                std::cout << "Taskqueue size: " << taskqueue_.size() << std::endl;
                task = taskqueue_.front();
                taskqueue_.pop();
            }
            if (task)
            {
                try
                {
                    task(); // task()中IO过程可以使用协程优化, 让出CPU资源
                }
                catch (std::bad_alloc &bad)
                {
                    std::cerr << "Bad_alloc caught in FixThreadPool::ThreadFunc task: " << bad.what() << std::endl;
                    while (true)
                        ;
                }
            }
        }
    }

    // 获取线程数量
    int GetThreadNum()
    {
        return threadnum_;
    }

private:
    bool started_;                          // 运行状态
    int threadnum_;                         // 线程数量
    std::vector<std::thread *> threadlist_; // 线程列表
    std::queue<Task> taskqueue_;            // 任务队列
    std::mutex mutex_;                      // 任务队列互斥锁
    std::condition_variable condition_;     // 任务队列同步的条件变量
};

class ThreadPool
{
public:
    using Task = std::function<void()>;

    ThreadPool(int min_threads = DEFAULT_THREAD_POOL_MIN_THREAD_NUM,
               int max_threads = DEFAULT_THREAD_POOL_MAX_THREAD_NUM,
               int max_idle_ms = DEFAULT_THREAD_POOL_MAX_IDLE_TIME) :
        min_thread_num(min_threads), max_thread_num(max_threads), max_idle_time(max_idle_ms), status(STOP), cur_thread_num(0), idle_thread_num(0)
    {
    }

    virtual ~ThreadPool()
    {
        stop();
    }

    void setMinThreadNum(int min_threads)
    {
        min_thread_num = min_threads;
    }
    void setMaxThreadNum(int max_threads)
    {
        max_thread_num = max_threads;
    }
    void setMaxIdleTime(int ms)
    {
        max_idle_time = ms;
    }
    int currentThreadNum()
    {
        return cur_thread_num;
    }
    int idleThreadNum()
    {
        return idle_thread_num;
    }
    size_t taskNum()
    {
        std::lock_guard<std::mutex> locker(task_mutex);
        return tasks.size();
    }
    bool isStarted()
    {
        return status != STOP;
    }
    bool isStopped()
    {
        return status == STOP;
    }

    int start(size_t start_threads = 0)
    {
        if (status != STOP)
            return -1;
        status = RUNNING;
        if (start_threads < min_thread_num)
            start_threads = min_thread_num;
        if (start_threads > max_thread_num)
            start_threads = max_thread_num;
        for (size_t i = 0; i < start_threads; ++i)
        {
            createThread();
        }
        return 0;
    }

    int stop()
    {
        if (status == STOP)
            return -1;
        status = STOP;
        task_cond.notify_all();
        for (auto &i : threads)
        {
            if (i.thread->joinable())
            {
                i.thread->join();
            }
        }
        threads.clear();
        cur_thread_num = 0;
        idle_thread_num = 0;
        return 0;
    }

    int pause()
    {
        if (status == RUNNING)
        {
            status = PAUSE;
        }
        return 0;
    }

    int resume()
    {
        if (status == PAUSE)
        {
            status = RUNNING;
        }
        return 0;
    }

    int wait()
    {
        while (status != STOP)
        {
            if (tasks.empty() && idle_thread_num == cur_thread_num)
            {
                break;
            }
            std::this_thread::yield();
        }
        return 0;
    }

    /*
     * return a future, calling future.get() will wait task done and return RetType.
     * commit(fn, args...)
     * commit(std::bind(&Class::mem_fn, &obj))
     * commit(std::mem_fn(&Class::mem_fn, &obj))
     *
     */
    template <class Fn, class... Args>
    auto commit(Fn &&fn, Args &&...args) -> std::future<decltype(fn(args...))>
    {
        if (status == STOP)
            start();
        if (idle_thread_num <= tasks.size() && cur_thread_num < max_thread_num)
        {
            createThread();
        }
        using RetType = decltype(fn(args...));
        auto task = std::make_shared<std::packaged_task<RetType()>>(
            std::bind(std::forward<Fn>(fn), std::forward<Args>(args)...));
        std::future<RetType> future = task->get_future();
        {
            std::lock_guard<std::mutex> locker(task_mutex);
            tasks.emplace([task]
                          {
                              (*task)();
                          });
        }

        task_cond.notify_one();
        return future;
    }

protected:
    bool createThread()
    {
        if (cur_thread_num >= max_thread_num)
            return false;
        std::thread *thread = new std::thread([this]
                                              {
                                                  while (status != STOP)
                                                  {
                                                      while (status == PAUSE)
                                                      {
                                                          std::this_thread::yield();
                                                      }

                                                      Task task;
                                                      {
                                                          std::unique_lock<std::mutex> locker(task_mutex);
                                                          task_cond.wait_for(locker, std::chrono::milliseconds(max_idle_time), [this]()
                                                                             {
                                                                                 return status == STOP || !tasks.empty();
                                                                             });
                                                          if (status == STOP)
                                                              return;
                                                          if (tasks.empty())
                                                          {
                                                              if (cur_thread_num > min_thread_num)
                                                              {
                                                                  delThread(std::this_thread::get_id());
                                                                  return;
                                                              }
                                                              continue;
                                                          }
                                                          --idle_thread_num;
                                                          task = std::move(tasks.front());
                                                          tasks.pop();
                                                      }
                                                      if (task)
                                                      {
                                                          task();
                                                          ++idle_thread_num;
                                                      }
                                                  }
                                              });
        addThread(thread);
        return true;
    }

    void addThread(std::thread *thread)
    {
        thread_mutex.lock();
        ++cur_thread_num;
        ++idle_thread_num;
        ThreadData data;
        data.thread = std::shared_ptr<std::thread>(thread);
        data.id = thread->get_id();
        data.status = RUNNING;
        data.start_time = std::chrono::steady_clock::now();
        data.stop_time = std::chrono::steady_clock::time_point();
        threads.emplace_back(data);
        thread_mutex.unlock();
    }

    void delThread(std::thread::id id)
    {
        auto now = std::chrono::steady_clock::now();
        thread_mutex.lock();
        --cur_thread_num;
        --idle_thread_num;
        auto iter = threads.begin();
        while (iter != threads.end())
        {
            if (iter->status == STOP && now > iter->stop_time)
            {
                if (iter->thread->joinable())
                {
                    iter->thread->join();
                    iter = threads.erase(iter);
                    continue;
                }
            }
            else if (iter->id == id)
            {
                iter->status = STOP;
                iter->stop_time = std::chrono::steady_clock::now();
            }
            ++iter;
        }
        thread_mutex.unlock();
    }

public:
    size_t min_thread_num;
    size_t max_thread_num;
    size_t max_idle_time;

protected:
    enum Status
    {
        STOP,
        RUNNING,
        PAUSE,
    };
    struct ThreadData
    {
        std::shared_ptr<std::thread> thread;
        std::thread::id id;
        Status status;
        std::chrono::steady_clock::time_point start_time;
        std::chrono::steady_clock::time_point stop_time;
    };
    std::atomic<Status> status;
    std::atomic<size_t> cur_thread_num;
    std::atomic<size_t> idle_thread_num;
    std::list<ThreadData> threads;
    std::mutex thread_mutex;
    std::queue<Task> tasks;
    std::mutex task_mutex;
    std::condition_variable task_cond;
};
