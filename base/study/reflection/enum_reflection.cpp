#include <iostream>
#include <string>

#define _TO_STR(x) #x
#define TO_STR(x)  _TO_STR(x)

// 非侵入式的编译器反射

// 获取数据类型 T 对应的字符串
template <class T>
std::string get_type_name()
{
    std::string s = __PRETTY_FUNCTION__;
    auto pos_begin = s.find("T = ");
    pos_begin += std::string("T = ").size();
    auto pos_end = s.find_first_of(";]", pos_begin);

    return s.substr(pos_begin, pos_end - pos_begin);
}

// 获取 T 类型的 N 变量 具体的值
template <class T, T N>
std::string get_int_name()
{
    std::string s = __PRETTY_FUNCTION__;
    auto pos_begin = s.find("N = ");
    pos_begin += std::string("N = ").size();
    auto pos_end = s.find_first_of(";]", pos_begin);

    return s.substr(pos_begin, pos_end - pos_begin);
}

template <int Beg, int End, class F, typename std::enable_if<Beg == End, int>::type = 0>
void static_for(F const &func)
{
    // 当 Beg 等于 End 时，函数体为空
}

template <int Beg, int End, class F, typename std::enable_if<Beg != End, int>::type = 0>
void static_for(F const &func)
{
    struct int_constant
    {
        enum
        {
            value = Beg
        };
    };

    func(int_constant());

    // 递归调用 static_for 直到 Beg 达到 End
    static_for<Beg + 1, End>(func);
}

template <class T, T N>
std::string get_enum_name_static()
{
    return __PRETTY_FUNCTION__;
}

template <int Beg = 0, int End = 256, class T>
std::string get_enum_name(T n)
{
    std::string s;
    static_for<Beg, End + 1>([&](auto i)
                             {
                                 if (n == (T)i.value)
                                 {
                                     s = get_enum_name_static<T, (T)i.value>();
                                 }
                             });

    if (s.empty())
    {
        return std::to_string(int(n));
    }

    auto pos_begin = s.find("N = ");
    pos_begin += std::string("N = ").size();
    auto pos_end = s.find_first_of(";]", pos_begin);

    s = s.substr(pos_begin, pos_end - pos_begin);

    // return s; // 此处返回 Color::RED

    auto sign = s.find("::");
    if (s.npos != sign)
    {
        s = s.substr(sign + std::string("::").size());
    }

    return s; // 此处返回 Red
}

template <class T, int Beg = 0, int End = 256>
T enum_from_name(std::string const &s)
{
    for (int i = Beg; i < End; ++i)
    {
        if (s == get_enum_name((T)i))
        {
            return (T)i;
        }
    }

    throw std::runtime_error("enum overflow");
}

// magic_enum实现原理 - 反射
// enum值 打印enum对应的枚举字符串
enum class Color
{
    WHITE,
    RED,
    GREEN,
    BLUE,
    YELLOW
};

void test_enum_refection()
{
    Color c = Color::YELLOW;
    std::cout << get_enum_name(c) << std::endl;                           // 已知 枚举值 获取 枚举名
    std::cout << (int)enum_from_name<Color>(TO_STR(YELLOW)) << std::endl; // 已知 枚举名 获取 枚举值
}

int main()
{
    test_enum_refection();

    return EXIT_SUCCESS;
}
