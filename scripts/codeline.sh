#!/bin/bash

if [ $# -eq 0 ]
then
  echo "usage: count_code_line.sh directory"
  exit 1
fi

CURRENT_DIR=$(cd `dirname $0`; pwd)
SOURCE_DIR=$1

# sudo apt install cloc
cloc $SOURCE_DIR
