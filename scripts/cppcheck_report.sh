#!/bin/bash

if [ $# -eq 0 ]
then
  echo "usage: cppcheck_report.sh directory"
  exit 1
fi

CURRENT_DIR=$(cd `dirname $0`; pwd)
SOURCE_DIR=$1
CPPCHECK_BUILD_DIR=$SOURCE_DIR/output/cppcheck
CPPCHECK_XML_FILE=$CPPCHECK_BUILD_DIR/cppcheck.xml

if [ -d $CPPCHECK_BUILD_DIR ];
then
  rm -rf $CPPCHECK_BUILD_DIR
fi
mkdir -p $CPPCHECK_BUILD_DIR
touch $CPPCHECK_XML_FILE

cppcheck \
--quiet \
--enable=all \
--xml \
--inconclusive \
--suppress=missingIncludeSystem \
--project=$SOURCE_DIR/build/compile_commands.json \
--output-file=$CPPCHECK_XML_FILE

cppcheck-htmlreport \
--file=$CPPCHECK_XML_FILE \
--title="cppcheck report" \
--report-dir=$CPPCHECK_BUILD_DIR \
--source-dir=$SOURCE_DIR
