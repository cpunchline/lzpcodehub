#!/bin/sh

top_path="$(git rev-parse --show-toplevel)"
cur_path=$(cd `dirname $0`; pwd)

# Apply commit message template
template_setting="$(git config commit.template)"
template_file="$cur_path/.gitmessage"
commitmsg_file="$cur_path/commit-msg"

echo "setting commit template to $template_file"
git config commit.template $template_file

echo "link commit-msg to $commitmsg_file"
rm -rf $top_path/.git/hooks/commit-msg
ln -s $commitmsg_file $top_path/.git/hooks/commit-msg
