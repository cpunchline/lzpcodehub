if(NOT WIN32)
    string(ASCII 27 Esc)
    set(ColorReset "${Esc}[m")
    set(ColorBold "${Esc}[1m")
    set(Red "${Esc}[31m")
    set(Green "${Esc}[32m")
    set(Yellow "${Esc}[33m")
    set(Blue "${Esc}[34m")
    set(Magenta "${Esc}[35m")
    set(Cyan "${Esc}[36m")
    set(White "${Esc}[37m")
    set(BoldRed "${Esc}[1;31m")
    set(BoldGreen "${Esc}[1;32m")
    set(BoldYellow "${Esc}[1;33m")
    set(BoldBlue "${Esc}[1;34m")
    set(BoldMagenta "${Esc}[1;35m")
    set(BoldCyan "${Esc}[1;36m")
    set(BoldWhite "${Esc}[1;37m")
endif()

include(CheckCCompilerFlag)
include(GNUInstallDirs)

# 获取系统和编译器所有信息
# cmake --system-information information.txt
cmake_host_system_information(RESULT SYSTEM_INFO QUERY OS_PLATFORM OS_NAME OS_RELEASE)
message(STATUS "${Yellow}${SYSTEM_INFO}${ColorReset}")
message(STATUS "${Yellow}${CMAKE_GENERATOR}${ColorReset}")

set(C_STANDARD 23)
set(CXX_STANDARD 26)
set(C_STANDARD_REQUIRED ON)
set(CXX_STANDARD_REQUIRED ON)

if(WIN32)
    add_definitions(-DNOMINMAX -D_USE_MATH_DEFINES)
endif()

if(NOT MSVC)
    find_program(CCACHE_PROGRAM ccache)
    if(CCACHE_PROGRAM)
        message(STATUS "${Green}Found CCache: ${CCACHE_PROGRAM} ${ColorReset}")
        set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE ${CCACHE_PROGRAM})
        set_property(GLOBAL PROPERTY RULE_LAUNCH_LINK ${CCACHE_PROGRAM})
        set(CMAKE_C_COMPILER_LAUNCHER ${CCACHE_PROGRAM})
        set(CMAKE_CXX_COMPILER_LAUNCHER ${CCACHE_PROGRAM})
    endif()
endif()

# 交叉编译
# 新式写法 cmake --toolchain toolchain_dir --install-prefix output_dir -DCMAKE_SYSROOT=sysroot_dir
# 老式写法 cmake -DCMAKE_TOOLCHAIN_FILE=toolchain_dir -DCMAKE_INSTALL_PREFILX=output_dir -DCMAKE_SYSROOT=sysroot_dir

# TOOLCHAIN.cmake
# set(CMAKE_SYSTEM_NAME Linux)
# set(CMAKE_SYSTEM_PROCESSOR processor_name) # arm aarch64

# get_filename_component(CURRENT_SCRIPT_DIR "${CMAKE_TOOLCHAIN_FILE}" DIRECTORY)

# set(CMAKE_C_COMPILER ${C_COMPILER_PATH} CACHE PATH "C compiler path" FORCE)
# set(CMAKE_CXX_COMPILER ${CXX_COMPILER_PATH} CACHE PATH "C++ compiler path" FORCE)

# set(CMAKE_C_COMPILER_WORKS ON)
# set(CMAKE_CXX_COMPILER_WORKS ON)

# set(CMAKE_SYSROOT sysroot_path)
# set(CMAKE_FIND_ROOT_PATH sysroot_path)

# NEVER 本地系统; ONLY 目标系统; BOTH 都用;
# set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM_ENV ONLY)
# set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
# set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
# set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

# set(CMAKE_C_FLAGS "-Werror -Wextra -Wall")
# set(CMAKE_CXX_FLAGS "-Werror -Wextra -Wall")
# set(CMAKE_SHARED_LINKER_FLAGS "-Wl,--as-needed -Wl,-z,noexecstack -Wl,-z,relro -Wl,-z,now")
# set(CMAKE_EXE_LINKER_FLAGS "-Wl,--as-needed -Wl,-z,noexecstack -Wl,-z,relro -Wl,-z,now")

# add_definitions(-Dxxx)

if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE Release)
endif()
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
# set(CMAKE_VERBOSE_MAKEFILE ON)

set(CMAKE_INSTALL_PREFIX ${CPUNCHLINE_OUTPUT_DIR})

# export LD_LIBRARY_PATH=~/CPUNCHLINE_HUB/output/lib/:$LD_LIBRARY_PATH
set(CMAKE_SKIP_BUILD_RPATH OFF) # 不要跳过构建时的RPATH设置; 编译生成的可执行文件或库将包含一个路径列表, 该列表指定了在运行时应查找动态库的位置
set(CMAKE_BUILD_WITH_INSTALL_RPATH ON) # CMake在构建(编译)时使用的RPATH应当与最终安装时的RPATH相同
set(CMAKE_INSTALL_RPATH_USE_LINK_PATH OFF) # 最终安装的RPATH时, 合并链接步骤中使用的库路径(比如指向本地构建的库或第三方库的临时安装目录), 启用这个选项可能会导致这些路径被包含到最终的RPATH中)
list(FIND CMAKE_PLATFORM_IMPLICIT_LINK_DIRECTORIES "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR}" isSystemDir)
if("${isSystemDir}" STREQUAL "-1")
    set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR}") # 确保在安装后, 程序能够正确地在指定的安装目录下查找动态链接库
    list(APPEND CMAKE_INSTALL_RPATH ":$ORIGIN")
endif("${isSystemDir}" STREQUAL "-1")

if(NOT DEFINED BUILD_SHARED_LIBS)
    set(BUILD_SHARED_LIBS ON) # 默认编译动态库
endif()

set(BUILD_STATIC_LIBS OFF) # 编译静态库

# clangd or clang-tidy 找不到系统头文件(交叉编译)
if(${CMAKE_EXPORT_COMPILE_COMMANDS})
    set(CMAKE_C_STANDARD_INCLUDE_DIRECTORIES ${CMAKE_C_STANDARD_INCLUDE_DIRECTORIES})
    set(CMAKE_CXX_STANDARD_INCLUDE_DIRECTORIES ${CMAKE_CXX_STANDARD_INCLUDE_DIRECTORIES})
endif()

# 动态库无法链接静态库
# 则让静态库编译也生成位置无关代码加-fPIC选项
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

# 仅设置某一个静态库, 对它启用位置无关的代码(PIC)
#[[
set_target_properties(xxx_static_lib PROPERTIES
    POSITION_INDEPENDENT_CODE ON                      # 启用位置无关
    WIN32_EXECUTABLE ON                               # Windows系统中写图形界面程序时, 运行时不启动控制台窗口, 只有GUI界面(默认 OFF)
    LINK_WHAT_YOU_USE ON                              # 告诉编译器不要自动剔除没有引用符号的链接库(默认 OFF)
    LINBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib # 动态库输出路径
    ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib  # 静态库输出路径
    RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin  # 可执行程序输出路径
    )
]]

#[[
# C头文件检测方法
include(CheckIncludeFiles)
CHECK_INCLUDE_FILES(stdint.h HAVE_STDINT_H)

# C++头文件检测方法
include(CheckIncludeFileCXX)
CHECK_INCLUDE_FILE_CXX(queue HAVE_QUEUE_H)

# 函数检测方法
include(CheckFunctionExists)
CHECK_FUNCTION_EXISTS(poll HAVE_POLL)

# 符号检测方法
include(CheckSymbolExists)
CHECK_SYMBOL_EXISTS(alloca "alloca.h" HAVE_ALLOCA)

# 库检测方法
find_library(HAVE_LIBRT rt)

include(CheckLibraryExists)
CHECK_LIBRARY_EXISTS(rt timer_gettime "" HAVE_LIBRT)
#]]


# install还要执行某些内容
# install(CODE "execute_process(COMMAND xxx)")
# install(SCRIPT ...)
