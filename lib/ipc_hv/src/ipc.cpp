#include "ipc.h"

typedef const struct _ipc_process_info_t
{
    uint32_t ipc_id;
    const char *process_name;
} ipc_process_info_t;

static const int thread_num = 3;
static hloop_t *accept_loop = NULL;
static hloop_t **worker_loops = NULL;
static hmutex_t s_mutex;
static const ipc_process_info_t s_ipc_process_info[] = {
    // 保证不同进程的进程名不能相同
    {E_IPC_ID_SERVER, "hv_ipc_server"},
    {E_IPC_ID_CLIENT, "hv_ipc_client"},
};
static unpack_setting_t ipc_unpack_setting;

static hloop_t *get_idle_loop()
{
    hloop_t *idle_loop = worker_loops[0];
    uint32_t min_nactives = hloop_nactives(worker_loops[0]);
    uint32_t each_nactives = 0;

    hmutex_lock(&s_mutex);
    for (int i = 1; i < thread_num; ++i)
    {
        each_nactives = hloop_nactives(worker_loops[i]);
        if (each_nactives < min_nactives)
        {
            min_nactives = each_nactives;
            idle_loop = worker_loops[i];
        }
    }
    hmutex_unlock(&s_mutex);

    return idle_loop;
}

void on_close(hio_t *io)
{
    PRINT_INFO("on_close fd[%d] error[%d]", hio_fd(io), hio_error(io));
}

void on_close_clear(hio_t *io)
{
    ipc_msg_t *p_ipc_data = (ipc_msg_t *)hevent_userdata(io);
    PRINT_INFO("on_close_clear fd[%d] error[%d]", hio_fd(io), hio_error(io));

    free(p_ipc_data);
}

void on_recv(hio_t *io, void *buf, int readbytes)
{
    ipc_msg_t *p_im_data = (ipc_msg_t *)buf;

    PRINT_INFO("on_recv fd[%d], readbytes[%d], msg_type[%u], src[%u]->dest[%u], msg_id[%u], timeout[%zu], send_len[%zu], recv_max_len[%zu]",
               hio_fd(io),
               readbytes,
               p_im_data->msg_type,
               p_im_data->src,
               p_im_data->dest,
               p_im_data->msg_id,
               p_im_data->timeout,
               p_im_data->send_len,
               p_im_data->recv_max_len);

    if (E_IPC_MSG_TYPE_NOTIFY != p_im_data->msg_type)
    {
        p_im_data->hd = (ipc_handle_t)io;
    }
    else
    {
        p_im_data->hd = NULL;
    }

    ipc_handle_cb_t ipc_cb = (ipc_handle_cb_t)hevent_userdata(io);
    if (NULL != ipc_cb)
    {
        ipc_cb(p_im_data);
    }

    hio_close(io);
}

void on_recv_async(hio_t *io, void *buf, int readbytes)
{
    ipc_msg_t *p_im_data = (ipc_msg_t *)buf;

    PRINT_INFO("on_recv_async fd[%d], readbytes[%d], msg_type[%u], src[%u]->dest[%u], msg_id[%u], timeout[%zu], send_len[%zu], recv_max_len[%zu]",
               hio_fd(io),
               readbytes,
               p_im_data->msg_type,
               p_im_data->src,
               p_im_data->dest,
               p_im_data->msg_id,
               p_im_data->timeout,
               p_im_data->send_len,
               p_im_data->recv_max_len);

    p_im_data->hd = (ipc_handle_t)io;

    ipc_msg_t *p_ipc_msg = (ipc_msg_t *)hevent_userdata(io);
    if (NULL != p_ipc_msg->async_cb)
    {
        p_ipc_msg->async_cb(p_im_data);
    }
    hio_close(io);
}

void on_write(hio_t *io, const void *buf, int writebytes)
{
#if 0
    if (!hio_write_is_complete(io))
    {
        return;
    }
#endif
    ipc_msg_t *p_im_data = (ipc_msg_t *)buf;
    PRINT_INFO("on_write fd[%d], writebytes[%d], msg_type[%u], src[%u]->dest[%u], msg_id[%u], timeout[%zu], send_len[%zu], recv_max_len[%zu]",
               hio_fd(io),
               writebytes,
               p_im_data->msg_type,
               p_im_data->src,
               p_im_data->dest,
               p_im_data->msg_id,
               p_im_data->timeout,
               p_im_data->send_len,
               p_im_data->recv_max_len);
    if (p_im_data->msg_type == E_IPC_MSG_TYPE_ASYNC)
    {
        hio_setcb_read(io, on_recv_async);
        hio_set_unpack(io, &ipc_unpack_setting);
        if (UINT32_MAX != p_im_data->timeout)
        {
            hio_set_read_timeout(io, p_im_data->timeout);
        }
        hio_read_until(io, sizeof(ipc_msg_t) + p_im_data->recv_max_len);
    }
    else if (p_im_data->msg_type == E_IPC_MSG_TYPE_RESP || p_im_data->msg_type == E_IPC_MSG_TYPE_NOTIFY)
    {
        hio_close(io);
    }
    else
    {
    }
}

void on_connect(hio_t *io)
{
    PRINT_INFO("on_connect fd[%d]", hio_fd(io));
    ipc_msg_t *p_ipc_msg = (ipc_msg_t *)hevent_userdata(io);
    hio_setcb_write(io, on_write);
    hio_set_write_timeout(io, IPC_WRITE_TIMEOUT);
    hio_write(io, p_ipc_msg, sizeof(ipc_msg_t) + p_ipc_msg->send_len);
}

void on_accept(hio_t *io)
{
    hio_detach(io);

    hloop_t *worker_loop = get_idle_loop();
    hevent_t ev;
    memset(&ev, 0, sizeof(ev));
    ev.loop = worker_loop;
    ev.cb = [](hevent_t *ev)
    {
        hloop_t *loop = ev->loop;
        hio_t *io = (hio_t *)hevent_userdata(ev);
        hio_attach(loop, io);

        PRINT_INFO("on_accept fd[%d]", hio_fd(io));
        hio_setcb_read(io, on_recv);
        hio_set_read_timeout(io, IPC_READ_TIMEOUT);
        hio_set_unpack(io, &ipc_unpack_setting);
        hio_read_once(io);
    };
    ev.userdata = io;
    hloop_post_event(worker_loop, &ev);
}

htimer_t *hv_timer_create(htimer_cb cb, unsigned int timeout, uint32_t repeat, void *timeoutdata)
{
    htimer_t *timer = htimer_add(get_idle_loop(), cb, timeout, repeat);
    if (NULL == timer)
    {
        return NULL;
    }
    hevent_set_userdata(timer, timeoutdata);

    return timer;
}

void hv_timer_destroy(htimer_t *timer)
{
    htimer_del(timer);
}

// 获取进程名的函数
static bool get_process_name(char *process_name)
{
    char path[PATH_MAX] = {0};
    ssize_t len = readlink("/proc/self/exe", path, sizeof(path) - 1);
    if (len != -1)
    {
        path[len] = '\0';
        strcpy(process_name, basename(path));
        PRINT_DEBUG("process_name[%s]-basename[%s]", path, process_name);
        return true;
    }

    PRINT_ERROR("readlink fail, errno[%d]", errno);

    return false;
}

static HTHREAD_ROUTINE(worker_thread)
{
    hloop_t *loop = (hloop_t *)userdata;
    prctl(PR_SET_NAME, "workers");
    PRINT_INFO("engine[%s]-pid[%ld]-tid[%ld]", hio_engine(), hloop_pid(loop), hloop_tid(loop));
    hloop_run(loop);
    return 0;
}

int ipc_init(ipc_handle_cb_t ipc_cb)
{
    static bool initflag = false;
    uint32_t ipc_id = E_IPC_ID_INVALID;
    char socket_path[UNIX_SOCKET_NAME_LEN_MAX] = {0};
    char process_name[PATH_MAX] = {0};
    size_t i = 0;

    if (initflag)
    {
        PRINT_ERROR("init again");
        return 0;
    }

    if (!get_process_name(process_name))
    {
        return false;
    }

    for (i = 0; i < sizeof(s_ipc_process_info) / sizeof(s_ipc_process_info[0]); i++)
    {
        if (0 == strcmp(process_name, s_ipc_process_info[i].process_name))
        {
            ipc_id = s_ipc_process_info[i].ipc_id;
            break;
        }
    }

    if (i >= sizeof(s_ipc_process_info) / sizeof(s_ipc_process_info[0]))
    {
        return -1;
    }

    snprintf(socket_path, UNIX_SOCKET_NAME_LEN_MAX, "%s/%u.ipc", UNIX_SOCKET_PATH_PREFIX, ipc_id);

    if (access(UNIX_SOCKET_PATH_PREFIX, F_OK) == 0)
    {
        if (access(socket_path, F_OK) == 0)
        {
            if (0 > unlink(socket_path))
            {
                return -1;
            }
        }
    }
    else
    {
        if (0 > mkdir(UNIX_SOCKET_PATH_PREFIX, UNIX_SOCKET_PATH_PREFIX_RIGHT))
        {
            PRINT_ERROR("mkdir fail, errno[%d]", errno);
            return -1;
        }
    }

    memset(&ipc_unpack_setting, 0, sizeof(unpack_setting_t));
    ipc_unpack_setting.mode = UNPACK_BY_LENGTH_FIELD;
    ipc_unpack_setting.package_max_length = DEFAULT_PACKAGE_MAX_LENGTH;
    ipc_unpack_setting.body_offset = sizeof(ipc_msg_t);
    ipc_unpack_setting.length_field_offset = sizeof(ipc_msg_t) - 2 * sizeof(size_t);
    ipc_unpack_setting.length_field_bytes = sizeof(size_t);
    ipc_unpack_setting.length_adjustment = 0;
    ipc_unpack_setting.length_field_coding = ENCODE_BY_LITTEL_ENDIAN;

    hmutex_init(&s_mutex);
    hmutex_lock(&s_mutex);
    worker_loops = (hloop_t **)malloc(sizeof(hloop_t *) * thread_num);
    for (i = 0; i < thread_num; ++i)
    {
        worker_loops[i] = hloop_new(HLOOP_FLAG_AUTO_FREE);
        hthread_create(worker_thread, worker_loops[i]);
    }
    hmutex_unlock(&s_mutex);

    accept_loop = hloop_new(HLOOP_FLAG_AUTO_FREE);
    if (NULL == accept_loop)
    {
        PRINT_ERROR("hloop_new fail!");
        return -1;
    }

    hio_t *listenio = hio_create_socket(accept_loop, socket_path, -1, HIO_TYPE_SOCK_STREAM, HIO_SERVER_SIDE);
    if (NULL == listenio)
    {
        PRINT_ERROR("hio_create_socket fail!");
        return -1;
    }

    hevent_set_userdata(listenio, ipc_cb);
    hio_setcb_close(listenio, on_close);
    hio_setcb_accept(listenio, on_accept);
    if (0 != hio_accept(listenio))
    {
        PRINT_ERROR("hio_accept fail!");
        return -1;
    }

    initflag = true;

    return 0;
}

int ipc_run()
{
    prctl(PR_SET_NAME, "acceptor");
    PRINT_INFO("engine[%s]-pid[%ld]-tid[%ld]", hio_engine(), hloop_pid(accept_loop), hloop_tid(accept_loop));
    return hloop_run(accept_loop);
}

void ipc_destroy()
{
    hmutex_lock(&s_mutex);
    for (int i = 0; i < thread_num; ++i)
    {
        hloop_free(&worker_loops[i]);
        free(worker_loops[i]);
        worker_loops[i] = NULL;
    }
    hmutex_unlock(&s_mutex);
    hmutex_destroy(&s_mutex);
    hloop_free(&accept_loop);
}

int ipc_send_notify(uint32_t src, uint32_t dest, uint32_t msg_id, const uint8_t *notify_data, size_t notify_len)
{
    size_t send_len = 0;
    char socket_path[UNIX_SOCKET_NAME_LEN_MAX] = {0};
    hio_t *connio = NULL;
    ipc_msg_t *p_notify_msg = NULL;

    send_len = sizeof(ipc_msg_t) + notify_len;
    p_notify_msg = (ipc_msg_t *)malloc(send_len);
    if (NULL == p_notify_msg)
    {
        return -1;
    }
    memset(p_notify_msg, 0, send_len);
    p_notify_msg->hd = connio;
    p_notify_msg->src = src;
    p_notify_msg->dest = dest;
    p_notify_msg->msg_type = E_IPC_MSG_TYPE_NOTIFY;
    p_notify_msg->msg_id = msg_id;
    p_notify_msg->async_cb = NULL;
    p_notify_msg->timeout = 0;
    p_notify_msg->send_len = notify_len;
    p_notify_msg->recv_max_len = 0;
    if (NULL != notify_data && p_notify_msg->send_len > 0)
    {
        memcpy(p_notify_msg->msg_data, notify_data, p_notify_msg->send_len);
    }

    snprintf(socket_path, UNIX_SOCKET_NAME_LEN_MAX, "%s/%u.ipc", UNIX_SOCKET_PATH_PREFIX, dest);
    connio = hio_create_socket(get_idle_loop(), socket_path, -1, HIO_TYPE_SOCK_STREAM, HIO_CLIENT_SIDE);
    if (NULL == connio)
    {
        PRINT_ERROR("hio_create_socket fail!");
        free(p_notify_msg);
        return -1;
    }
    hevent_set_userdata(connio, p_notify_msg);
    hio_setcb_close(connio, on_close_clear);
    hio_setcb_connect(connio, on_connect);
    hio_set_connect_timeout(connio, IPC_CONNECT_TIMEOUT);
    hio_connect(connio);

    return 0;
}

int ipc_send_async_req(uint32_t src, uint32_t dest, uint32_t msg_id, const uint8_t *req_data, size_t req_len, size_t resp_max_len, ipc_handle_cb_t ipc_async_cb, uint32_t timeout)
{
    size_t send_len = 0;
    char socket_path[UNIX_SOCKET_NAME_LEN_MAX] = {0};
    hio_t *connio = NULL;
    ipc_msg_t *p_async_msg = NULL;

    send_len = sizeof(ipc_msg_t) + req_len;
    p_async_msg = (ipc_msg_t *)malloc(send_len);
    if (NULL == p_async_msg)
    {
        return -1;
    }
    memset(p_async_msg, 0, send_len);
    p_async_msg->hd = connio;
    p_async_msg->src = src;
    p_async_msg->dest = dest;
    p_async_msg->msg_type = E_IPC_MSG_TYPE_ASYNC;
    p_async_msg->msg_id = msg_id;
    p_async_msg->async_cb = ipc_async_cb;
    p_async_msg->timeout = timeout;
    p_async_msg->send_len = req_len;
    p_async_msg->recv_max_len = resp_max_len;
    if (NULL != req_data && p_async_msg->send_len > 0)
    {
        memcpy(p_async_msg->msg_data, req_data, p_async_msg->send_len);
    }

    snprintf(socket_path, UNIX_SOCKET_NAME_LEN_MAX, "%s/%u.ipc", UNIX_SOCKET_PATH_PREFIX, dest);
    connio = hio_create_socket(get_idle_loop(), socket_path, -1, HIO_TYPE_SOCK_STREAM, HIO_CLIENT_SIDE);
    if (NULL == connio)
    {
        PRINT_ERROR("hio_create_socket fail!");
        free(p_async_msg);
        return -1;
    }
    hevent_set_userdata(connio, p_async_msg);
    hio_setcb_close(connio, on_close_clear);
    hio_setcb_connect(connio, on_connect);
    hio_set_connect_timeout(connio, IPC_CONNECT_TIMEOUT);
    hio_connect(connio);

    return 0;
}

int select_read_fd_timeout(int rfd, int ms, int max_retries = 2)
{
    struct timeval tv = {ms / 1000, (ms % 1000) * 1000};
    fd_set readfds;
    int retries = 0;

    while (retries < max_retries)
    {
        FD_ZERO(&readfds);
        FD_SET(rfd, &readfds);
        int ret = select(rfd + 1, &readfds, NULL, NULL, &tv);
        if (ret > 0)
        {
            return 0;
        }
        if (ret == 0)
        {
            errno = ETIMEDOUT;
            return -1;
        }
        if (errno != EINTR && errno != EAGAIN)
        {
            PRINT_ERROR("select fail, errno[%d]", errno);
            return -1;
        }
        retries++;
    }
    return -1;
}

int ipc_send_sync_req(uint32_t src, uint32_t dest, uint32_t msg_id, const uint8_t *req_data, size_t req_len, uint8_t *resp_data, size_t *resp_len, uint32_t timeout)
{
    if (NULL == resp_len)
    {
        return -1;
    }

    int ret = -1;
    ssize_t nrecv = -1;
    size_t send_len = 0;
    size_t recv_len = 0;
    char socket_path[UNIX_SOCKET_NAME_LEN_MAX] = {0};
    hio_t *connio = NULL;
    ipc_msg_t *p_sync_req_msg = NULL;
    ipc_msg_t *p_sync_resp_msg = NULL;

    send_len = sizeof(ipc_msg_t) + req_len;
    p_sync_req_msg = (ipc_msg_t *)malloc(send_len);
    if (NULL == p_sync_req_msg)
    {
        return -1;
    }
    memset(p_sync_req_msg, 0, send_len);
    p_sync_req_msg->hd = connio;
    p_sync_req_msg->src = src;
    p_sync_req_msg->dest = dest;
    p_sync_req_msg->msg_type = E_IPC_MSG_TYPE_SYNC;
    p_sync_req_msg->msg_id = msg_id;
    p_sync_req_msg->async_cb = NULL;
    p_sync_req_msg->timeout = timeout;
    p_sync_req_msg->send_len = req_len;
    p_sync_req_msg->recv_max_len = *resp_len;
    if (NULL != req_data && p_sync_req_msg->send_len > 0)
    {
        memcpy(p_sync_req_msg->msg_data, req_data, p_sync_req_msg->send_len);
    }

    snprintf(socket_path, UNIX_SOCKET_NAME_LEN_MAX, "%s/%u.ipc", UNIX_SOCKET_PATH_PREFIX, dest);
    connio = hio_create_socket(get_idle_loop(), socket_path, -1, HIO_TYPE_SOCK_STREAM, HIO_CLIENT_SIDE);
    if (NULL == connio)
    {
        PRINT_ERROR("hio_create_socket fail!");
        free(p_sync_req_msg);
        return -1;
    }
    hevent_set_userdata(connio, p_sync_req_msg);
    hio_setcb_close(connio, on_close_clear);
    hio_setcb_connect(connio, on_connect);
    hio_set_connect_timeout(connio, IPC_CONNECT_TIMEOUT);
    hio_connect(connio);

    ret = select_read_fd_timeout(hio_fd(connio), timeout);
    if (ret < 0)
    {
        PRINT_ERROR("select_read_fd_timeout fail!");
        if (ETIMEDOUT == errno)
        {
            ret = -ETIMEDOUT;
        }
        hio_close(connio);
        return ret;
    }

    recv_len = sizeof(ipc_msg_t) + p_sync_req_msg->recv_max_len;
    p_sync_resp_msg = (ipc_msg_t *)malloc(recv_len);
    if (NULL == p_sync_resp_msg)
    {
        hio_close(connio);
        return -1;
    }
    memset(p_sync_resp_msg, 0x00, recv_len);

    nrecv = recv(hio_fd(connio), (char *)p_sync_resp_msg, recv_len, 0);
    if (nrecv <= 0)
    {
        PRINT_ERROR("socket_recv fail, ret[%zd]!", nrecv);
        hio_close(connio);
        free(p_sync_resp_msg);
        return -1;
    }

    if (p_sync_resp_msg->send_len > p_sync_req_msg->recv_max_len)
    {
        PRINT_ERROR("p_sync_resp_msg->send_len[%zu] > p_sync_req_msg->recv_max_len[%zu]!", p_sync_resp_msg->send_len, p_sync_req_msg->recv_max_len);
        hio_close(connio);
        free(p_sync_resp_msg);
    }

    if (NULL != resp_data && p_sync_resp_msg->send_len > 0)
    {
        memcpy(resp_data, p_sync_resp_msg->msg_data, p_sync_resp_msg->send_len);
    }

    hio_close(connio);
    free(p_sync_resp_msg);

    return 0;
}

int ipc_send_resp(const ipc_msg_t *req, const uint8_t *resp_data, size_t resp_len)
{
    if (NULL == req)
    {
        return -1;
    }

    size_t send_len = 0;
    ipc_msg_t *p_sync_resp_msg = NULL;
    hio_t *connio = NULL;

    if (resp_len > req->recv_max_len)
    {
        PRINT_ERROR("resp_len[%zu] > req->recv_max_len[%zu]!", resp_len, req->recv_max_len);
        return -1;
    }

    send_len = sizeof(ipc_msg_t) + resp_len;
    p_sync_resp_msg = (ipc_msg_t *)malloc(send_len);
    if (NULL == p_sync_resp_msg)
    {
        return -1;
    }
    memset(p_sync_resp_msg, 0, send_len);
    p_sync_resp_msg->hd = connio;
    p_sync_resp_msg->src = req->dest;
    p_sync_resp_msg->dest = req->src;
    p_sync_resp_msg->msg_type = E_IPC_MSG_TYPE_RESP;
    p_sync_resp_msg->msg_id = req->msg_id;
    p_sync_resp_msg->async_cb = NULL;
    p_sync_resp_msg->timeout = 0;
    p_sync_resp_msg->send_len = resp_len;
    p_sync_resp_msg->recv_max_len = 0; // no need
    if (NULL != resp_data && p_sync_resp_msg->send_len > 0)
    {
        memcpy(p_sync_resp_msg->msg_data, resp_data, p_sync_resp_msg->send_len);
    }

    connio = (hio_t *)req->hd;
    hio_setcb_write(connio, on_write);
    hio_set_write_timeout(connio, IPC_WRITE_TIMEOUT);
    hio_write(connio, (char *)p_sync_resp_msg, send_len);

    return 0;
}
