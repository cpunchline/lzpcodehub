#include <string.h>
#include <sys/time.h>
#include <pthread.h>
#include "zmq.h"
#include "ipc_zmq.h"

#define WL_IPC_ZMQ_ERROR(formart, ...) printf(formart, ##__VA_ARGS__)
#define WL_IPC_ZMQ_WARN(formart, ...)  printf(formart, ##__VA_ARGS__)
#define WL_IPC_ZMQ_INFO(formart, ...)  printf(formart, ##__VA_ARGS__)
#define WL_IPC_ZMQ_DEBUG(formart, ...) printf(formart, ##__VA_ARGS__)

static ipc_zmq_manager_info_t *g_ipc_minfo_array[E_IPC_ZMQ_MODULE_ID_MAX] = {0};

static void get_zmq_lib_version()
{
    int major = -1, minor = -1, patch = -1;
    zmq_version(&major, &minor, &patch);
    WL_IPC_ZMQ_INFO("ZeroMQ Version[%d.%d.%d]", major, minor, patch);
}

static int32_t ipc_zmq_poll(void *socket, long timeout)
{
    if (NULL == socket)
    {
        WL_IPC_ZMQ_ERROR("invalid params!");
        return -1;
    }

    int32_t ret = -1;
    int32_t zmq_poll_ret = -1;
    int32_t delta_time = 0;
    struct timeval start = {0};
    struct timeval end = {0};

#if ZMQ_VERSION < 40201
    zmq_pollitem_t items[] = {
        {
         socket,
         0,
         ZMQ_POLLIN | ZMQ_POLLERR,
         0,
         },
    };
#else
    zmq_pollitem_t items[] = {
        {
         socket,
         0,
         ZMQ_POLLIN | ZMQ_POLLERR | ZMQ_POLLPRI,
         0,
         },
    };
#endif

    while (1)
    {
        gettimeofday(&start, 0);

        zmq_poll_ret = zmq_poll(items, sizeof(items) / sizeof(items[0]), timeout);
        if (zmq_poll_ret < 0)
        {
            WL_IPC_ZMQ_ERROR("zmq_poll fail, errno=%d(%s), zmq_errno=%d(%s)", errno, strerror(errno), zmq_errno(), zmq_strerror(zmq_errno()));

            if (EINTR == errno)
            {
                continue;
            }

            gettimeofday(&end, 0);

            delta_time = IPC_ZMQ_S_TO_MS * end.tv_sec + end.tv_usec / IPC_ZMQ_MS_TO_US - (IPC_ZMQ_S_TO_MS * start.tv_sec + start.tv_usec / IPC_ZMQ_MS_TO_US);
            if (delta_time >= timeout)
            {
                WL_IPC_ZMQ_ERROR("abort to avoid infinite waiting ...");
                ret = -1;
                break;
            }
            timeout -= delta_time;
            WL_IPC_ZMQ_DEBUG("continue to wait for %ld ms", timeout);
        }
        else
        {
            if (0 == zmq_poll_ret)
            {
                WL_IPC_ZMQ_DEBUG("poll timeout, revents[%u]", items[0].revents);
                ret = -8;
            }
            else
            {
                if (items[0].revents & ZMQ_POLLIN)
                {
                    ret = 0;
                }
                else
                {
                    WL_IPC_ZMQ_ERROR("poll timeout, zmq_poll_ret[%d], revents[%u]", zmq_poll_ret, items[0].revents);
                    ret = -1;
                }
            }
            break;
        }
    }

    return ret;
}

void ipc_zmq_msg_free(void *data_, void *hint_)
{
    (void)hint_;
    if (NULL != data_)
    {
        free(data_);
        data_ = NULL;
    }
}

int32_t response_worker(void *rep, ipc_zmq_manager_info_t *pinfo)
{
    int32_t ret = -1;
    zmq_msg_t request;
    ipc_zmq_msg_baseinfo_t msg_baseinfo = {};

    zmq_msg_init(&request);
    ret = zmq_msg_recv(&request, rep, 0);
    if (-1 == ret)
    {
        WL_IPC_ZMQ_ERROR("zmq_msg_recv fail, errno[%d](%s), zmq_errno[%d](%s)!", errno, strerror(errno), zmq_errno(), zmq_strerror(zmq_errno()));
        zmq_msg_close(&request);
        return -1;
    }
    memcpy(&msg_baseinfo, zmq_msg_data(&request), zmq_msg_size(&request));
    zmq_msg_close(&request);

    zmq_msg_init(&request);
    ret = zmq_msg_recv(&request, rep, 0);
    if (-1 == ret || errno != EAGAIN)
    {
        WL_IPC_ZMQ_ERROR("zmq_msg_recv fail, errno[%d](%s), zmq_errno[%d](%s)!", errno, strerror(errno), zmq_errno(), zmq_strerror(zmq_errno()));
        zmq_msg_close(&request);
        return -1;
    }

    int64_t more = 0;
    size_t len = sizeof(more);
    zmq_getsockopt(rep, ZMQ_RCVMORE, &more, &len);
    if (0 != more)
    {
        return -1;
    }

    zmq_msg_t reply;

    if (NULL != pinfo->reg_info.response_handler && (E_IPC_ZMQ_MSG_TYPE_SYNC == msg_baseinfo.msg_type || E_IPC_ZMQ_MSG_TYPE_ASYNC == msg_baseinfo.msg_type))
    {
        if (0 != msg_baseinfo.recv_len)
        {
            void *out_struct = (void *)malloc(msg_baseinfo.recv_len);
            if (NULL == out_struct)
            {
                WL_IPC_ZMQ_ERROR("malloc fail, errno[%d](%s)!", errno, strerror(errno));
                zmq_msg_close(&request);
                return -1;
            }
            memset(out_struct, 0x00, msg_baseinfo.recv_len);
            pinfo->reg_info.response_handler(msg_baseinfo.src_id, msg_baseinfo.msg_id, zmq_msg_data(&request), out_struct);
            zmq_msg_init_data(&reply, out_struct, msg_baseinfo.recv_len, ipc_zmq_msg_free, NULL);
            ret = zmq_msg_send(&reply, rep, 0);
            if (-1 == ret)
            {
                WL_IPC_ZMQ_ERROR("zmq_msg_send fail, errno[%d](%s), zmq_errno[%d](%s)!", errno, strerror(errno), zmq_errno(), zmq_strerror(zmq_errno()));
                zmq_msg_close(&reply);
            }
            if (NULL != out_struct)
            {
                free(out_struct);
                out_struct = NULL;
            }
        }
        else
        {
            pinfo->reg_info.response_handler(msg_baseinfo.src_id, msg_baseinfo.msg_id, zmq_msg_data(&request), NULL);
            zmq_msg_init(&reply);
            ret = zmq_msg_send(&reply, rep, 0);
            if (-1 == ret)
            {
                WL_IPC_ZMQ_ERROR("zmq_msg_send fail, errno[%d](%s), zmq_errno[%d](%s)!", errno, strerror(errno), zmq_errno(), zmq_strerror(zmq_errno()));
                zmq_msg_close(&reply);
            }
        }
    }
    else if (NULL != pinfo->reg_info.notify_handler && E_IPC_ZMQ_MSG_TYPE_NOTIFY == msg_baseinfo.msg_type)
    {
        pinfo->reg_info.notify_handler(msg_baseinfo.src_id, msg_baseinfo.msg_id, zmq_msg_data(&request), zmq_msg_size(&request));
        zmq_msg_init(&reply);
        ret = zmq_msg_send(&reply, rep, 0);
        if (-1 == ret)
        {
            WL_IPC_ZMQ_ERROR("zmq_msg_send fail, errno[%d](%s), zmq_errno[%d](%s)!", errno, strerror(errno), zmq_errno(), zmq_strerror(zmq_errno()));
            zmq_msg_close(&reply);
        }
    }
    else
    {
        zmq_msg_init(&reply);
        ret = zmq_msg_send(&reply, rep, 0);
        if (-1 == ret)
        {
            WL_IPC_ZMQ_ERROR("zmq_msg_send fail, errno[%d](%s), zmq_errno[%d](%s)!", errno, strerror(errno), zmq_errno(), zmq_strerror(zmq_errno()));
            zmq_msg_close(&reply);
        }
    }

    zmq_msg_close(&reply);
    zmq_msg_close(&request);

    return ret;
}

static void ipc_zmq_single_wait_response_handler(void *arg)
{
    int32_t ret = -1;
    char address[D_IPC_ZMQ_SOCKET_ADDRESS_LEN_MAX] = {0};
    ipc_zmq_manager_info_t *pinfo = (ipc_zmq_manager_info_t *)arg;

    pinfo->rep = zmq_socket(pinfo->ctx, ZMQ_REP);
    if (NULL == pinfo->rep)
    {
        WL_IPC_ZMQ_ERROR("zmq_socket fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        return;
    }

    snprintf(address, sizeof(address), D_IPC_ZMQ_REQ_RESP_ADDRESS, pinfo->reg_info.module_id);
    ret = zmq_bind(pinfo->rep, address);
    if (-1 == ret)
    {
        WL_IPC_ZMQ_ERROR("zmq_bind fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        return;
    }

    while (pinfo->is_run)
    {
        ret = ipc_zmq_poll(pinfo->rep, D_IPC_ZMQ_WAIT_CONTINUE);
        WL_IPC_ZMQ_ERROR("ipc_zmq_poll ret[%d]", ret);
        if (0 != ret)
        {
            break;
        }

        response_worker(pinfo->rep, pinfo);
    }
}

static void *ipc_zmq_reponse_worker(void *arg)
{
    int32_t ret = -1;
    pthread_detach(pthread_self());

    ipc_zmq_manager_info_t *pinfo = (ipc_zmq_manager_info_t *)arg;

    void *rep = zmq_socket(pinfo->ctx, ZMQ_REP);
    if (NULL == rep)
    {
        WL_IPC_ZMQ_ERROR("zmq_socket fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        return NULL;
    }

    ret = zmq_connect(rep, pinfo->mult_reqbackaddress);
    if (-1 == ret)
    {
        WL_IPC_ZMQ_ERROR("zmq_connect fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        return NULL;
    }

    while (pinfo->is_run)
    {
        ret = ipc_zmq_poll(rep, D_IPC_ZMQ_WAIT_CONTINUE);
        WL_IPC_ZMQ_DEBUG("ipc_zmq_poll ret[%d]", ret);
        if (0 != ret)
        {
            break;
        }

        response_worker(rep, pinfo);
    }

    zmq_close(rep);

    return NULL;
}

static void ipc_zmq_mult_wait_response_handler(void *arg)
{
    int32_t ret = -1;
    char address_front[D_IPC_ZMQ_SOCKET_ADDRESS_LEN_MAX] = {0};
    ipc_zmq_manager_info_t *pinfo = (ipc_zmq_manager_info_t *)arg;

    void *frontend = zmq_socket(pinfo->ctx, ZMQ_ROUTER);
    if (NULL == frontend)
    {
        WL_IPC_ZMQ_ERROR("zmq_socket fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        return;
    }

    snprintf(address_front, sizeof(address_front), D_IPC_ZMQ_REQ_RESP_ADDRESS, pinfo->reg_info.module_id);
    ret = zmq_bind(frontend, address_front);
    if (-1 == ret)
    {
        WL_IPC_ZMQ_ERROR("zmq_bind fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        return;
    }

    void *backend = zmq_socket(pinfo->ctx, ZMQ_DEALER);
    if (NULL == backend)
    {
        WL_IPC_ZMQ_ERROR("zmq_socket fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        return;
    }

    snprintf(pinfo->mult_reqbackaddress, sizeof(pinfo->mult_reqbackaddress), D_INPROC_ZMQ_BACK_REQ_RESP_ADDRESS,
             pinfo->reg_info.module_id);
    ret = zmq_bind(backend, pinfo->mult_reqbackaddress);
    if (-1 == ret)
    {
        WL_IPC_ZMQ_ERROR("zmq_bind fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        return;
    }

    for (size_t i = 0; i < 3; ++i)
    {
        pthread_t tid = 0;
        ret = pthread_create(&tid, NULL, ipc_zmq_reponse_worker, pinfo);
        if (0 != ret)
        {
            WL_IPC_ZMQ_ERROR("pthread_create fail, errno[%d](%s)!", errno, strerror(errno));
            break;
        }
    }

    ret = zmq_proxy(frontend, backend, NULL);
    if (-1 == ret)
    {
        WL_IPC_ZMQ_ERROR("zmq_proxy fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
    }
}

static void ipc_zmq_async_wait_response_handler(void *arg)
{
    int ret = -1;
    char address_front[D_IPC_ZMQ_SOCKET_ADDRESS_LEN_MAX] = {};
    char address_req[D_IPC_ZMQ_SOCKET_ADDRESS_LEN_MAX] = {};
    ipc_zmq_msg_baseinfo_t msg_baseinfo = {};

    ipc_zmq_manager_info_t *pinfo = (ipc_zmq_manager_info_t *)arg;
    void *dealer = zmq_socket(pinfo->ctx, ZMQ_DEALER);
    if (NULL == dealer)
    {
        WL_IPC_ZMQ_ERROR("zmq_socket fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        return;
    }

    snprintf(address_front, sizeof(address_front), D_INPROC_ZMQ_ASYNC_DEALER_ADDRESS, pinfo->reg_info.module_id);
    ret = zmq_bind(dealer, address_front);
    if (-1 == ret)
    {
        WL_IPC_ZMQ_ERROR("zmq_bind fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        return;
    }

    while (pinfo->is_run)
    {
        zmq_msg_t dealer_msg;
        zmq_msg_init(&dealer_msg);
        ret = zmq_msg_recv(&dealer_msg, dealer, 0);
        if (-1 == ret)
        {
            WL_IPC_ZMQ_ERROR("zmq_msg_recv fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
            zmq_msg_close(&dealer_msg);
            break;
        }
        memcpy(&msg_baseinfo, zmq_msg_data(&dealer_msg), zmq_msg_size(&dealer_msg));

        void *async_req = zmq_socket(pinfo->ctx, ZMQ_REQ);
        if (NULL == async_req)
        {
            WL_IPC_ZMQ_ERROR("zmq_socket fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
            zmq_msg_close(&dealer_msg);
            break;
        }

        snprintf(address_req, sizeof(address_req), D_IPC_ZMQ_REQ_RESP_ADDRESS, msg_baseinfo.dest_id);
        ret = zmq_connect(async_req, address_req);
        if (-1 == ret)
        {
            WL_IPC_ZMQ_ERROR("zmq_connect fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
            zmq_msg_close(&dealer_msg);
            break;
        }

        ret = ipc_zmq_poll(async_req, D_IPC_ZMQ_ASYNC_WAIT_TIMEOUT);
        WL_IPC_ZMQ_DEBUG("ipc_zmq_poll ret[%d]", ret);

        ret = zmq_msg_send(&dealer_msg, async_req, ZMQ_SNDMORE);
        if (-1 == ret)
        {
            WL_IPC_ZMQ_ERROR("zmq_msg_send fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
            zmq_msg_close(&dealer_msg);
            break;
        }
        zmq_msg_close(&dealer_msg);

        zmq_msg_init(&dealer_msg);
        ret = zmq_msg_recv(&dealer_msg, dealer, 0);
        if (-1 == ret)
        {
            WL_IPC_ZMQ_ERROR("zmq_msg_recv fail, errno[%d](%s), zmq_errno[%d](%s)!", errno, strerror(errno), zmq_errno(), zmq_strerror(zmq_errno()));
            zmq_msg_close(&dealer_msg);
            break;
        }
        ret = zmq_msg_send(&dealer_msg, async_req, 0);
        if (-1 == ret)
        {
            WL_IPC_ZMQ_ERROR("zmq_msg_send fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
            zmq_msg_close(&dealer_msg);
            break;
        }
        zmq_msg_close(&dealer_msg);

        ret = ipc_zmq_poll(async_req, D_IPC_ZMQ_ASYNC_CONNECT_TIMEOUT);
        WL_IPC_ZMQ_DEBUG("ipc_zmq_poll ret[%d]", ret);
        if (0 == ret)
        {
            zmq_msg_t async_resp_msg;
            zmq_msg_init(&async_resp_msg);
            ret = zmq_msg_recv(&async_resp_msg, async_req, 0);
            if (-1 == ret)
            {
                WL_IPC_ZMQ_ERROR("zmq_msg_recv fail, errno[%d](%s), zmq_errno[%d](%s)!", errno, strerror(errno), zmq_errno(), zmq_strerror(zmq_errno()));
                zmq_msg_close(&async_resp_msg);
                break;
            }

            pinfo->reg_info.async_handler(msg_baseinfo.dest_id, msg_baseinfo.msg_id, zmq_msg_data(&async_resp_msg));
            zmq_msg_close(&async_resp_msg);
        }
        zmq_close(async_req);
    }
}

static void *ipc_zmq_broadcast_proxy_handler(void *arg)
{
    int ret = -1;
    void *frontend = NULL;
    void *backend = NULL;

    void *ctx = arg;
    if (NULL != ctx)
    {
        frontend = zmq_socket(ctx, ZMQ_XSUB);
        if (NULL == frontend)
        {
            WL_IPC_ZMQ_ERROR("zmq_socket fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
            return NULL;
        }

        ret = zmq_bind(frontend, D_IPC_ZMQ_BROADCAST_FRONTEND_ADDRESS);
        if (-1 == ret)
        {
            WL_IPC_ZMQ_ERROR("zmq_bind fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
            return NULL;
        }

        backend = zmq_socket(ctx, ZMQ_XPUB);
        if (NULL == backend)
        {
            WL_IPC_ZMQ_ERROR("zmq_socket fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
            return NULL;
        }

        ret = zmq_bind(backend, D_IPC_ZMQ_BROADCAST_BACKEND_ADDRESS);
        if (-1 == ret)
        {
            WL_IPC_ZMQ_ERROR("zmq_bind fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
            return NULL;
        }

        ret = zmq_proxy(frontend, backend, NULL);
        if (-1 == ret)
        {
            WL_IPC_ZMQ_ERROR("zmq_proxy fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
            return NULL;
        }
    }

    zmq_close(frontend);
    zmq_close(backend);

    return NULL;
}

static void *ipc_zmq_broadcast_subscriber_handler(void *arg)
{
    int ret = -1;
    zmq_msg_t message;
    ipc_zmq_broadcast_msg_baseinfo_t broadcast_baseinfo = {};
    ipc_zmq_manager_info_t *pinfo = (ipc_zmq_manager_info_t *)arg;

    void *subscriber = zmq_socket(pinfo->ctx, ZMQ_SUB);
    if (NULL == subscriber)
    {
        WL_IPC_ZMQ_ERROR("zmq_socket fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        return NULL;
    }

    ret = zmq_connect(subscriber, D_IPC_ZMQ_BROADCAST_BACKEND_ADDRESS);
    if (-1 == ret)
    {
        WL_IPC_ZMQ_ERROR("zmq_connect fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        return NULL;
    }

    ret =
        zmq_setsockopt(subscriber, ZMQ_SUBSCRIBE, D_IPC_ZMQ_BROADCAST_TOPIC_STR, strlen(D_IPC_ZMQ_BROADCAST_TOPIC_STR));
    if (-1 == ret)
    {
        WL_IPC_ZMQ_ERROR("zmq_setsockopt fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        return NULL;
    }

    while (pinfo->is_run)
    {
        memset(&broadcast_baseinfo, 0x00, sizeof(broadcast_baseinfo));
        ret = ipc_zmq_poll(subscriber, D_IPC_ZMQ_WAIT_CONTINUE);
        WL_IPC_ZMQ_DEBUG("ipc_zmq_poll ret[%d]", ret);

        zmq_msg_init(&message);
        ret = zmq_msg_recv(&message, subscriber, 0);
        if (-1 == ret && EAGAIN != errno)
        {
            WL_IPC_ZMQ_ERROR("zmq_msg_recv fail, errno[%d](%s), zmq_errno[%d](%s)!", errno, strerror(errno), zmq_errno(), zmq_strerror(zmq_errno()));
            zmq_msg_close(&message);
            break;
        }
        zmq_msg_close(&message);

        zmq_msg_init(&message);
        ret = zmq_msg_recv(&message, subscriber, 0);
        if (-1 == ret)
        {
            WL_IPC_ZMQ_ERROR("zmq_msg_recv fail, errno[%d](%s), zmq_errno[%d](%s)!", errno, strerror(errno), zmq_errno(), zmq_strerror(zmq_errno()));
            zmq_msg_close(&message);
            break;
        }
        memcpy(&broadcast_baseinfo, zmq_msg_data(&message), zmq_msg_size(&message));
        zmq_msg_close(&message);

        zmq_msg_init(&message);
        ret = zmq_msg_recv(&message, subscriber, 0);
        if (-1 == ret)
        {
            WL_IPC_ZMQ_ERROR("zmq_msg_recv fail, errno[%d](%s), zmq_errno[%d](%s)!", errno, strerror(errno), zmq_errno(), zmq_strerror(zmq_errno()));
            zmq_msg_close(&message);
            break;
        }

        if (NULL != pinfo->reg_info.broadcast_handler)
        {
            if (0 != zmq_msg_size(&message))
            {
                pinfo->reg_info.broadcast_handler(broadcast_baseinfo.src_id, broadcast_baseinfo.msg_id, zmq_msg_data(&message));
            }
            else
            {
                pinfo->reg_info.broadcast_handler(broadcast_baseinfo.src_id, broadcast_baseinfo.msg_id, NULL);
            }
        }
        zmq_msg_close(&message);
    }

    return NULL;
}

int32_t ipc_zmq_init(ipc_zmq_register_info_t *reg_info)
{
    int32_t ret = -1;
    ipc_zmq_manager_info_t *pinfo = NULL;

    get_zmq_lib_version();

    if (NULL == reg_info)
    {
        WL_IPC_ZMQ_ERROR("invalid params!");
        return -1;
    }

    if (NULL != g_ipc_minfo_array[reg_info->module_id])
    {
        WL_IPC_ZMQ_ERROR("ipc inited!");
        return -1;
    }

    pinfo = (ipc_zmq_manager_info_t *)malloc(sizeof(ipc_zmq_manager_info_t));
    if (NULL == pinfo)
    {
        WL_IPC_ZMQ_ERROR("malloc fail, errno[%d](%s)!", errno, strerror(errno));
        return -1;
    }
    memset(pinfo, 0x00, sizeof(ipc_zmq_manager_info_t));
    g_ipc_minfo_array[reg_info->module_id] = pinfo;
    memcpy(&pinfo->reg_info, reg_info, sizeof(ipc_zmq_register_info_t));

    pinfo->is_run = true;

    pinfo->ctx = zmq_ctx_new();
    if (NULL == pinfo->ctx)
    {
        WL_IPC_ZMQ_ERROR("zmq_ctx_new fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        return -1;
    }

    pinfo->pub = zmq_socket(pinfo->ctx, ZMQ_PUB);
    if (NULL == pinfo->pub)
    {
        WL_IPC_ZMQ_ERROR("zmq_socket fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        return -1;
    }

    if (NULL != reg_info->broadcast_handler)
    {
        pthread_t broadcast_sub_tid = 0;
        if (E_IPC_ZMQ_MODULE_ID_MANAGER == reg_info->module_id)
        {
            pthread_t broadcast_proxy_tid = 0;
            ret = pthread_create(&broadcast_proxy_tid, 0, ipc_zmq_broadcast_proxy_handler, pinfo->ctx);
            if (0 != ret)
            {
                WL_IPC_ZMQ_ERROR("pthread_create fail, errno[%d](%s)!", errno, strerror(errno));
                return -1;
            }
        }

        ret = pthread_create(&broadcast_sub_tid, 0, ipc_zmq_broadcast_subscriber_handler, pinfo);
        if (0 != ret)
        {
            WL_IPC_ZMQ_ERROR("pthread_create fail, errno[%d](%s)!", errno, strerror(errno));
            return -1;
        }
    }

    ret = zmq_connect(pinfo->pub, D_IPC_ZMQ_BROADCAST_FRONTEND_ADDRESS); // connect with manager module
    if (-1 == ret)
    {
        WL_IPC_ZMQ_ERROR("zmq_connect fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        return -1;
    }
    ret = ipc_zmq_poll(pinfo->pub, D_IPC_ZMQ_BROADCAST_CONNECT_TIMEOUT);
    WL_IPC_ZMQ_DEBUG("ipc_zmq_poll ret[%d]", ret);

    if (NULL != reg_info->response_handler)
    {
        if (E_IPC_ZMQ_WORKMODE_SINGLE_WORKER == reg_info->work_mode)
        {
            pinfo->rep_thread = zmq_threadstart(&ipc_zmq_single_wait_response_handler, pinfo);
        }
        else
        {
            pinfo->rep_thread = zmq_threadstart(&ipc_zmq_mult_wait_response_handler, pinfo);
        }

        if (NULL == pinfo->rep_thread)
        {
            WL_IPC_ZMQ_ERROR("zmq_threadstart fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
            return -1;
        }
    }

    pinfo->async_thread = zmq_threadstart(&ipc_zmq_async_wait_response_handler, pinfo);
    if (NULL == pinfo->async_thread)
    {
        WL_IPC_ZMQ_ERROR("zmq_threadstart fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        return -1;
    }

    return 0;
}

int32_t ipc_zmq_destroy(uint32_t module_id)
{
    if (module_id <= E_IPC_ZMQ_MODULE_ID_INVALID || module_id >= E_IPC_ZMQ_MODULE_ID_MAX)
    {
        return -1;
    }

    ipc_zmq_manager_info_t *pinfo = g_ipc_minfo_array[module_id];
    if (NULL != pinfo)
    {
        zmq_ctx_shutdown(pinfo->ctx);
        zmq_threadclose(pinfo->rep_thread);
        zmq_threadclose(pinfo->async_thread);
        free(pinfo);
        pinfo = NULL;
        g_ipc_minfo_array[module_id] = NULL;
    }

    return 0;
}

int32_t ipc_zmq_send_notify(uint32_t src_id, uint32_t dest_id, uint32_t msg_id, const void *notify_data,
                            size_t notify_data_len)
{
    int32_t ret = -1;
    char address[D_IPC_ZMQ_SOCKET_ADDRESS_LEN_MAX] = {0};
    ipc_zmq_msg_baseinfo_t msg_baseinfo = {};
    ipc_zmq_manager_info_t *pinfo = g_ipc_minfo_array[src_id];

    void *notify = zmq_socket(pinfo->ctx, ZMQ_REQ);
    if (NULL == notify)
    {
        WL_IPC_ZMQ_ERROR("zmq_socket fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        return -1;
    }

    snprintf(address, sizeof(address), D_IPC_ZMQ_REQ_RESP_ADDRESS, dest_id);

    ret = zmq_connect(notify, address);
    if (-1 == ret)
    {
        WL_IPC_ZMQ_ERROR("zmq_connect fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        zmq_close(notify);
        return -1;
    }

    zmq_msg_t notify_msg;

    msg_baseinfo.src_id = src_id;
    msg_baseinfo.dest_id = dest_id;
    msg_baseinfo.msg_type = E_IPC_ZMQ_MSG_TYPE_NOTIFY;
    msg_baseinfo.msg_id = msg_id;
    msg_baseinfo.send_len = notify_data_len;
    msg_baseinfo.recv_len = 0;

    zmq_msg_init_data(&notify_msg, (void *)&msg_baseinfo, sizeof(msg_baseinfo), NULL, NULL);
    ret = zmq_msg_send(&notify_msg, notify, ZMQ_SNDMORE);
    if (-1 == ret)
    {
        WL_IPC_ZMQ_ERROR("zmq_msg_send fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        zmq_msg_close(&notify_msg);
        zmq_close(notify);
        return -1;
    }
    zmq_msg_close(&notify_msg);

    if (notify_data_len > 0)
    {
        zmq_msg_init_size(&notify_msg, notify_data_len);
        memcpy(zmq_msg_data(&notify_msg), notify_data, notify_data_len);
    }
    else
    {
        zmq_msg_init(&notify_msg);
    }

    ret = zmq_msg_send(&notify_msg, notify, ZMQ_DONTWAIT);
    if (-1 == ret)
    {
        WL_IPC_ZMQ_ERROR("zmq_msg_send fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        zmq_msg_close(&notify_msg);
        zmq_close(notify);
        return -1;
    }
    zmq_msg_close(&notify_msg);

    ret = ipc_zmq_poll(notify, D_IPC_ZMQ_NOTIFY_WAIT_TIMEOUT);
    WL_IPC_ZMQ_DEBUG("ipc_zmq_poll ret[%d]", ret);
    if (0 == ret)
    {
        zmq_msg_t notify_resp_msg;
        zmq_msg_init(&notify_resp_msg);
        ret = zmq_msg_recv(&notify_resp_msg, notify, 0);
        if (-1 == ret)
        {
            WL_IPC_ZMQ_ERROR("zmq_msg_recv fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        }
        zmq_msg_close(&notify_resp_msg);
    }

    zmq_disconnect(&notify, address);
    zmq_close(notify);

    return 0;
}

int32_t ipc_zmq_send_sync(uint32_t src_id, uint32_t dest_id, uint32_t msg_id, const void *sync_req_data,
                          size_t sync_req_data_len, void *sync_resp_data, size_t *sync_resp_data_len,
                          size_t sync_resp_data_max_len, long timeout)
{
    int32_t ret = -1;
    char address[D_IPC_ZMQ_SOCKET_ADDRESS_LEN_MAX] = {0};
    ipc_zmq_msg_baseinfo_t msg_baseinfo = {};
    zmq_msg_t sync_msg;
    ipc_zmq_manager_info_t *pinfo = g_ipc_minfo_array[src_id];

    void *req = zmq_socket(pinfo->ctx, ZMQ_REQ);
    if (NULL == req)
    {
        WL_IPC_ZMQ_ERROR("zmq_socket fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        return -1;
    }

    snprintf(address, sizeof(address), D_IPC_ZMQ_REQ_RESP_ADDRESS, dest_id);
    ret = zmq_connect(req, address);
    if (-1 == ret)
    {
        WL_IPC_ZMQ_ERROR("zmq_connect fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        return -1;
    }

    msg_baseinfo.src_id = src_id;
    msg_baseinfo.dest_id = dest_id;
    msg_baseinfo.msg_type = E_IPC_ZMQ_MSG_TYPE_SYNC;
    msg_baseinfo.msg_id = msg_id;
    msg_baseinfo.send_len = sync_req_data_len;
    msg_baseinfo.recv_len = sync_resp_data_max_len;

    zmq_msg_init_data(&sync_msg, &msg_baseinfo, sizeof(msg_baseinfo), NULL, NULL);
    ret = zmq_msg_send(&sync_msg, req, ZMQ_SNDMORE);
    if (-1 == ret)
    {
        WL_IPC_ZMQ_ERROR("zmq_msg_send fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        zmq_msg_close(&sync_msg);
        zmq_close(req);
        return -1;
    }
    zmq_msg_close(&sync_msg);

    if (sync_req_data_len > 0)
    {
        zmq_msg_init_size(&sync_msg, sync_req_data_len);
        memcpy(zmq_msg_data(&sync_msg), sync_req_data, sync_req_data_len);
    }
    else
    {
        zmq_msg_init(&sync_msg);
    }

    ret = zmq_msg_send(&sync_msg, req, ZMQ_DONTWAIT);
    if (-1 == ret)
    {
        WL_IPC_ZMQ_ERROR("zmq_msg_send fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        zmq_msg_close(&sync_msg);
        zmq_close(req);
        return -1;
    }
    zmq_msg_close(&sync_msg);

    ret = ipc_zmq_poll(req, timeout);
    WL_IPC_ZMQ_DEBUG("ipc_zmq_poll ret[%d]", ret);
    if (0 == ret)
    {
        zmq_msg_t sync_resp_msg;
        zmq_msg_init(&sync_resp_msg);
        ret = zmq_msg_recv(&sync_resp_msg, req, 0);
        if (-1 == ret)
        {
            WL_IPC_ZMQ_ERROR("zmq_msg_recv fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        }

        if (zmq_msg_size(&sync_resp_msg) > sync_resp_data_max_len)
        {
            WL_IPC_ZMQ_ERROR("sync resp is over limit[%zu]!", zmq_msg_size(&sync_resp_msg));
            zmq_msg_close(&sync_resp_msg);
            return -1;
        }

        *sync_resp_data_len = zmq_msg_size(&sync_resp_msg);
        memcpy(sync_resp_data, zmq_msg_data(&sync_resp_msg), *sync_resp_data_len);
        zmq_msg_close(&sync_resp_msg);
    }

    zmq_close(req);

    return ret;
}

int32_t ipc_zmq_send_async(uint32_t src_id, uint32_t dest_id, uint32_t msg_id, const void *async_req_data,
                           size_t async_req_data_len, size_t async_resp_data_len)
{
    int32_t ret = -1;
    char address[D_IPC_ZMQ_SOCKET_ADDRESS_LEN_MAX] = {0};
    ipc_zmq_msg_baseinfo_t msg_baseinfo = {};
    zmq_msg_t async_msg;
    ipc_zmq_manager_info_t *pinfo = g_ipc_minfo_array[src_id];

    void *dealer = zmq_socket(pinfo->ctx, ZMQ_DEALER);
    if (NULL == dealer)
    {
        WL_IPC_ZMQ_ERROR("zmq_socket fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        return -1;
    }

    snprintf(address, sizeof(address), D_INPROC_ZMQ_ASYNC_DEALER_ADDRESS, src_id);
    ret = zmq_connect(dealer, address);
    if (-1 == ret)
    {
        WL_IPC_ZMQ_ERROR("zmq_connect fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        return -1;
    }

    msg_baseinfo.src_id = src_id;
    msg_baseinfo.dest_id = dest_id;
    msg_baseinfo.msg_type = E_IPC_ZMQ_MSG_TYPE_ASYNC;
    msg_baseinfo.msg_id = msg_id;
    msg_baseinfo.send_len = async_req_data_len;
    msg_baseinfo.recv_len = async_resp_data_len;

    zmq_msg_init_data(&async_msg, (void *)&msg_baseinfo, sizeof(msg_baseinfo), NULL, NULL);
    ret = zmq_msg_send(&async_msg, dealer, ZMQ_SNDMORE);
    if (-1 == ret)
    {
        WL_IPC_ZMQ_ERROR("zmq_msg_send fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        zmq_msg_close(&async_msg);
        zmq_close(dealer);
        return -1;
    }
    zmq_msg_close(&async_msg);

    if (async_req_data_len > 0)
    {
        zmq_msg_init_size(&async_msg, async_req_data_len);
        memcpy(zmq_msg_data(&async_msg), async_req_data, async_req_data_len);
    }
    else
    {
        zmq_msg_init(&async_msg);
    }

    ret = zmq_msg_send(&async_msg, dealer, 0);
    if (-1 == ret)
    {
        WL_IPC_ZMQ_ERROR("zmq_msg_send fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        zmq_msg_close(&async_msg);
        zmq_close(dealer);
        return -1;
    }

    ret = ipc_zmq_poll(dealer, D_IPC_ZMQ_ASYNC_WAIT_TIMEOUT);
    WL_IPC_ZMQ_DEBUG("ipc_zmq_poll ret[%d]", ret);

    zmq_msg_close(&async_msg);
    zmq_close(dealer);

    return 0;
}

int32_t ipc_zmq_send_broadcast(uint32_t src_id, uint32_t msg_id, const void *broadcast_data, size_t broadcast_data_len)
{
    int32_t ret = -1;
    const uint8_t topic_buff[] = {D_IPC_ZMQ_BROADCAST_TOPIC_STR};
    ipc_zmq_broadcast_msg_baseinfo_t bmsg_baseinfo = {};
    ipc_zmq_manager_info_t *pinfo = g_ipc_minfo_array[src_id];

    bmsg_baseinfo.sub = 1;
    bmsg_baseinfo.msg_id = msg_id;
    bmsg_baseinfo.src_id = src_id;
    bmsg_baseinfo.msg_type = E_IPC_ZMQ_MSG_TYPE_BROADCAST;

    ret = zmq_send(pinfo->pub, topic_buff, sizeof(topic_buff), ZMQ_SNDMORE);
    if (-1 == ret)
    {
        WL_IPC_ZMQ_ERROR("zmq_send fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        return -1;
    }

    ret = zmq_send(pinfo->pub, &bmsg_baseinfo, sizeof(bmsg_baseinfo), ZMQ_SNDMORE);
    if (-1 == ret)
    {
        WL_IPC_ZMQ_ERROR("zmq_send fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        return -1;
    }

    ret = zmq_send(pinfo->pub, &broadcast_data, broadcast_data_len, 0);
    if (-1 == ret)
    {
        WL_IPC_ZMQ_ERROR("zmq_send fail, zmq_errno[%d](%s)!", zmq_errno(), zmq_strerror(zmq_errno()));
        return -1;
    }

    return 0;
}
