# C++ 调用 MYSQL API 连接池

`sudo apt install mysql-server mysql-client libmysqlclient-dev`

## 高并发下频繁处理瓶颈
* 建立通信: `TCP三次握手`
* 数据库服务器的`连接认证`
* 服务器`关闭连接`的资源回收
* `断开`通信的TCP四次挥手

> 如果客户端和服务端`频繁`进行类似操纵, 影响整个`开发效率`

## 数据库连接池
> 为了`提高`数据库(关系型数据库)的访问`瓶颈`, 除在服务器端添加缓存服务器缓存常用的数据, 还可添加连接池来提高服务器访问效率

## 创建连接池思路
![连接池URML](https://mmbiz.qpic.cn/mmbiz_png/ORog4TEnkbt72TibPibqUOnXvuG7dQ3WYYmP1QVHBJDhYJ9FB3DS6GJVib2ibQKQVIiaiaVfug7kiaOMLRBbE1nNYRzew/0?wx_fmt=png "连接池UML")
>连接池主要用于`网络服务器端`, 用于同时接受`多个用户端`请求, 数据库与数据库客户端采用`TCP通信`.

* 数据库客户端和服务端先建立起`多个连接`
* 多线程通过`套接字通信`取出连接池中的一个连接, 然后和服务器直接进行通信, 通信之后再将此连接`还给连接池`(减少数据库连接和断开的次数)
* 数据库连接池对应C++中的一个数据库连接对象, 即`单例模式`
* 连接池中包括数据库服务器连接对应的IP, 端口, 用户, 密码等信息
* 对数据库对象存入`STL`当中, 需要设置最大值, 最小值限制队列
* 多线程从连接池中取出数据库对象若有取出, `没有等待`调用算法
* 对 连接池中的数据库连接(空间时间长的即调度算法)进行`适当`断开连接
* 共享资源的访问, 需要`互斥锁`(生产者消费者问题)

## 单例模式
* `懒汉模式`
> 当使用这个类的时候才创建它
> 创建对象时, 加锁保证有且仅有一个
> (有线程安全问题)

* `饿汉模式`
> 不管用不用它, 只要类被创建, 这个实例就有
> 没有线程安全问题

## 连接池算法实现C++
> 换将配置参考`jsoncpp`和`MySQL API`文章
[Jsoncpp配置](https://www.blog.foryouos.cn/computer-science/cpp/course-3/C%E5%92%8CcppMySQL%20API%E8%B0%83%E7%94%A8/)
[MySQl配置](https://www.blog.foryouos.cn/computer-science/cpp/course-3/jsoncpp%E5%A4%84%E7%90%86Json%E6%95%B0%E6%8D%AE/)

![连接池](https://mmbiz.qpic.cn/mmbiz_jpg/ORog4TEnkbvjQPcRPuH2CcuicpSLUXpTeGKvufeV604g8HohMsTqHcXp0ibY0kHjqCOv0ds0LaskbgiaAMiaelLBAA/0?wx_fmt=jpeg "连接池")


参考资料:
* [B站爱编程的大丙](https://www.bilibili.com/video/BV1Fr4y1s7w4/)
* 《深入设计模式》-亚历山大什韦茨
