#include <iostream>

#include "MysqlConn.hpp"

using std::cout;
using std::endl;

// 初始化数据库连接
MysqlConn::MysqlConn()
{
    m_conn = mysql_init(nullptr);            // 初始化MySQL
    mysql_set_character_set(m_conn, "utf8"); // 设置MySQL的格式字符集GBK
}

// 释放数据库连接
MysqlConn::~MysqlConn()
{
    if (m_conn != nullptr)
    {
        mysql_close(m_conn);
    }
    freeResult();
}

// 连接数据库
bool MysqlConn::connect(string user, string passwd, string dbName, string ip, unsigned short port)
{
    MYSQL *ptr = mysql_real_connect(m_conn, ip.c_str(), user.c_str(), passwd.c_str(), dbName.c_str(), port, nullptr, 0);
    // 如果连接成功返回ture, 失败返回false
    return nullptr != ptr;
}

// 更新数据库:insert, update, delete
bool MysqlConn::update(string sql)
{
    // query执行成功返回0
    try
    {
        if (mysql_query(m_conn, sql.c_str()))
        {
            throw invalid_argument("update fail!");
            return false;
        };
    }
    catch (exception &e)
    {
        cout << e.what() << endl;
    }

    return true;
}

// 查询数据库
bool MysqlConn::query(string sql)
{
    freeResult(); // 释放之前查询的结果集
    try
    {
        if (mysql_query(m_conn, sql.c_str()))
        {
            throw invalid_argument("query fail!");
            return false;
        };
        m_result = mysql_store_result(m_conn); // 获取查询结果
    }
    catch (exception &e)
    {
        cout << e.what() << endl;
    }

    return true;
}

// 遍历查询得到的结果集
bool MysqlConn::next()
{
    if (m_result != nullptr)
    {
        m_row = mysql_fetch_row(m_result);
        if (m_row != nullptr)
        {
            return true;
        }
    }
    return false;
}

// 得到结果集中的字段值
string MysqlConn::value(int index)
{
    // 表示列的数量
    int rowCount = mysql_num_fields(m_result); // 函数得到结果集中的列数
    if (index >= rowCount || index < 0)
    {
        return string();
    }
    char *val = m_row[index];                                    // 若为二进制数据, 中间是有"\0"的
    unsigned long length = mysql_fetch_lengths(m_result)[index]; // 传入length就不会以"\0"为结束符, 而是通过长度把对应的字符转换为string类型
    return string(val, length);
}

// 事务操作
bool MysqlConn::transaction()
{
    return mysql_autocommit(m_conn, false);
}

// 提交事务
bool MysqlConn::commit()
{
    return mysql_commit(m_conn);
}

// 事务回滚
bool MysqlConn::rollback()
{
    return mysql_rollback(m_conn);
}

// 刷新起始的空闲时间点
void MysqlConn::refreshAliveTime()
{
    // 这个时间戳就是某个数据库连接, 它起始存活的时间点
    // 这个时间点通过时间类就可以得到了
    m_aliveTime = steady_clock::now();
}

// 计算连接存活的总时长
long long MysqlConn::getAliveTime()
{
    nanoseconds duration = steady_clock::now() - m_aliveTime;
    milliseconds millsec = duration_cast<milliseconds>(duration);
    return millsec.count();
}

void MysqlConn::freeResult()
{
    if (nullptr != m_result)
    {
        mysql_free_result(m_result);
        m_result = nullptr;
    }
}
