set(DEBUG_BACKTRACE_APP debug_backtrace_app)

aux_source_directory(. DEBUG_BACKTRACE_APP_SRC_LIST)

add_executable(${DEBUG_BACKTRACE_APP} ${DEBUG_BACKTRACE_APP_SRC_LIST})
target_include_directories(${DEBUG_BACKTRACE_APP} PUBLIC .)
target_link_libraries(${DEBUG_BACKTRACE_APP} PUBLIC dl)

install(TARGETS ${DEBUG_BACKTRACE_APP} DESTINATION bin)
